<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 29/10/16
 * Time: 12:25
 *
 	#Request

	URL -> http://api.fast-medicine.com/{endpoint}

	REQUEST ->
		- session: not mandatory
		- exec:
	```
	{
		"action": <action>,
		"args":<args>
	}
	```

	#Response

	RESPONSE -> SUCCESS
	```
	{
		"response" : <response>,
		"status"   : "success",
		"error"    : <error>
	}
	```

	RESPONSE -> ERROR

	```
	{
		"status"   : "error",
		"code"     : <code>,
		"error"    : <error>
	}
	```
 *
 */

namespace api;

class Api {

	const RESPONSE		= 'response';
	const CODE			= 'code';
	const ERROR			= 'error';

	const HTTP_CODE_OK		=	200;
	const HTTP_CODE_CREATED	=	201;

	const HTTP_CODE_BAD_REQUEST	= 400;
	const HTTP_CODE_NOT_FOUND	= 404;

	const HTTP_CODE_INTERNAL_SERVER_ERROR	= 500;
	const HTTP_CODE_NOT_IMPLEMENTED			= 501;
	const HTTP_CODE_BAD_GATEWAY				= 502;
	const HTTP_CODE_SERVICE_UNAVAILABLE		= 503;
	const HTTP_CODE_TIME_OUT				= 504;
	const HTTP_CODE_VERSION_NOT_SUPPORTED	= 505;

	const HTTP_MESSAGE_OK						= 'OK';
	const HTTP_MESSAGE_CREATED					= 'CREATED';
	const HTTP_MESSAGE_BAD_REQUEST				= 'BAD REQUEST';
	const HTTP_MESSAGE_NOT_FOUND				= 'NOT FOUND';
	const HTTP_MESSAGE_INTERNAL_SERVER_ERROR	= 'INTERNAL_SERVER_ERROR';
	const HTTP_MESSAGE_NOT_IMPLEMENTED			= 'NOT_IMPLEMENTED';
	const HTTP_MESSAGE_SERVICE_UNAVAILABLE		= 'SERVICE UNAVAILABLE';
	const HTTP_MESSAGE_TIME_OUT					= 'TIME OUT';
	const HTTP_MESSAGE_VERSION_NOT_SUPPORTED	= 'VERSION NOT SUPPORTED';

	const CONFIG = "config/dev/config.ini";

	private function __construct($di) {
		$this->config 		= new \Phalcon\Config\Adapter\Ini(self::CONFIG);
		$this->microService	= new \Phalcon\Mvc\Micro($di);
	}

	/**
	 * @return Api
	 */
	public static function getInstance($di) {

		if (!isset(self::$instance)) {
			$api = __CLASS__;
			self::$instance = new $api($di);
		}

		return self::$instance;
	}

	/**
	 * Send request prepare response
	 */
	public function sendRequest() {

        foreach($this->config->collections as $collection ) {

        	self::$response = [];

        	try {

        		$collectionInstance =
					\api\collections\BaseCollection::createFromConfig(
						$collection
					);

				$this->microService->mount(
					$collectionInstance
				);

			} catch (\Exception $exception) {
				$this->sendError($exception);
			}
		}

		try {
			$this->buildResponse(
				$this->microService->handle()
			);
		} catch (\Exception $e) {
			$this->sendError($e);
		}

		$this->microService->response->send();

		$this->microService->notFound(function () {
			$this->microService->response->setStatusCode(
				self::HTTP_CODE_NOT_FOUND,
				self::HTTP_MESSAGE_NOT_FOUND
			)->sendHeaders();
			echo 'This is crazy, but this page was not found!';
		});
    }

	/**
	 * @param \Exception $exception
	 * @internal param \Phalcon $app
	 */
	protected function sendError(
		\Exception $exception
	) {
		self::$response[self::CODE]		= $exception->getCode();
		self::$response[self::ERROR]	= $exception->getMessage();

		$this->microService->response->setJsonContent(
			self::$response
		);

		$this->microService->response->setStatusCode(
			self::$response[self::CODE],
			self::$response[self::ERROR]
		);

		$this
			->microService
			->response
			->setHeader('Access-Control-Allow-Origin', '*');

		$this->microService->response->send();
	}

	/**
	 * @param $response
	 */
	protected function buildResponse($response) {

		self::$response[self::RESPONSE]	= $response;

		$this->microService->response->setJsonContent(
			self::$response
		);

		$this->microService->response->setStatusCode(
			self::HTTP_CODE_OK,
			self::HTTP_MESSAGE_OK
		);

		$this
			->microService
			->response
			->setHeader('Access-Control-Allow-Origin', '*');
	}

	/**
	 * @var Api
	 */
	public static $instance;

	/**
	 * @var \Phalcon\Config\Adapter\Ini
	 */
	private $config;

	/**
	 * @var \Phalcon\Mvc\Micro
	 */
	private $microService;

	/**
	 * @var array
	 */
    public static $response;

}