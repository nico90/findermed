<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 17/02/17
 * Time: 19:25
 */

namespace api;


abstract class Factory
{
    abstract static function onCreate(string $class, array $args = null);
}