<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 08/09/17
 * Time: 11:02
 */

namespace auth;


class Credential {

	const FIELD_USERNAME = 'username';
	const FIELD_PASSWORD = 'password';

	function __construct(
		array $userData
	) {

		if(
			is_string($userData[self::FIELD_USERNAME]) &&
			is_string($userData[self::FIELD_PASSWORD])
		) {

			$this->username = $userData[self::FIELD_USERNAME];
			$this->password = $userData[self::FIELD_PASSWORD];

		} else {

			throw new \api\exception\ApiException('Credential incorrect');
		}

	}

	/**
	 * @return string
	 *
	 * @TODO ya espero el md5, no hay que convertirlo aca.
	 */
	public function getPassword() : string {
		return md5($this->password);
	}

	/**
	 * @return string
	 */
	public function getUsername() : string {
		return $this->username;
	}

	/**
	 * @param string $username
	 */
	public function setUsername($username) {
		$this->username = $username;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * @param \auth\Session $session
	 */
	public function setSession(\auth\Session $session) {
		$this->session = $session;
	}

	/** @var  string */
	private $username;

	/** @var  string */
	private $password;

	/** @var  \auth\Session */
	private $session;


}