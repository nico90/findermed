<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 15/07/17
 * Time: 20:12
 */

namespace auth;

use Firebase\JWT\JWT;

class Session {

	/**
	 * I need remove logic driver and write responsability only session.
	 * Create Class Driver and drive logic here.
	 *
	 * And create token, the token can save in redis, but have that be
	 *
	 * think about that retrive driver for here and not create since other place
	 *
	*/


	/**
	 * in future, receive string for set cache, but this logic should't be here
	 */
	private function __construct() {

		$this->driver 	= \cache\Driver::createFromConfig('redis');
		$this->config 	= new \Phalcon\Config\Adapter\Ini(\api\Api::CONFIG);
	}

	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}


	/**
	 * @param array $user
	 *
	 * @return Session
	 */

	public function createSession(
		array $user
	) {

		$session = $this->getToken($user);

		$key = $this->buildKeySession();

		$this->driver->set( $key, $user );

		$this->driver->expire($key);

		return $session;

	}

	/**
	 * refresh key in redis.
	 * @param int $uid
	 */
	public function refreshSession() {
		$key = $this->buildKeySession();
		$this->driver->expire($key);
	}

	/**
	 * @param $tokenId
	 * @return string
	 */
	private function buildKeySession() {
		return sprintf(
			\api\controller\login\LoginController::REDIS_KEY_LOGIN,
			$this->sessionToken
		);
	}

	/**
	 * @param array $user
	 * @return Session
	 */
	private function getToken(array $user) : self {

		$this->sessionToken    = md5(microtime() . bin2hex(random_bytes(8)));

		$issuedAt   = time();
		$expire     = $issuedAt + 3600;         // Adding 1hours
		$serverName = $_SERVER['SERVER_NAME'];

		$data = [
			'iat'	=> $issuedAt,         // Issued at: time when the token was generated
			'jti'	=> $this->sessionToken,          // Json Token Id: an unique identifier for the token
			'iss'	=> $serverName,       // Issuer
			'exp'	=> $expire,
			'uid'	=> $user['id'],
			'data'	=> $user
		];

		$this->token = JWT::encode(
			$data,
			$this->config->secretkey->secret
		);

		return $this;
	}

	private static $instance = NULL;

	/** @var  string */
	public $token;

	/** @var  string */
	public $sessionToken;

	/** @var  \cache\Driver */
	private $driver;

	/**
	 * @var \Phalcon\Config\Adapter\Ini
	 */
	private $config;


}