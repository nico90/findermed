<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 19/09/17
 * Time: 16:45
 */

namespace cache;


class Driver {

	/*
	 * @todo: Make generic Driver for use APC, Redis, o anywhere lib cache.
	 * */

	const CONFIG_HOST = 'host';

	/**
	 * @param string $driver
	 * @return Driver
	 * @throws \Phalcon\Cache\Exception
	 */
	public static function createFromConfig(string $driver = 'redis') {
		if(in_array($driver, self::$driverAvalaible)) {

			$config = new \Phalcon\Config\Adapter\Ini(\api\Api::CONFIG);

			return new self($config, $driver);

		} else {
			throw new \Phalcon\Cache\Exception('Driver not found');
		}
	}

	/**
	 * Driver constructor.
	 * @param \Phalcon\Config\Adapter\Ini $config
	 * @param string $driver
	 */
	public function __construct(
		\Phalcon\Config\Adapter\Ini $config,
		string $driver
	) {
		self::$redis = new \Redis();
		self::$redis->connect($config->$driver[self::CONFIG_HOST]);
	}

	/**
	 * @param $key
	 * @param $user
	 */
	public function set(
		string $key,
		array $param
	){
		self::$redis->hMset($key, $param);
	}

	/**
	 * @param $key
	 * @param $fields
	 * @return array
	 */
	public function get(
		string $key,
		array $fields
	) : array {
		return self::$redis->hMGet($key, $fields);
	}

	/**
	 * @param string $key
	 * @return bool
	 * @internal param $fields
	 */
	public function keyExist(
		string $key
	) : bool {
		return self::$redis->exists($key);
	}

	/**
	 * @param string $key
	 * @return bool
	 * @internal param $fields
	 */
	public function delete(
		string $key
	) {
		self::$redis->delete($key);
	}


	/**
	 * @param string $key
	 * @param int $ttl
	 */
	public function expire(string $key, int $ttl = 3600) {
		self::$redis->expire($key, $ttl);
	}


	private static $driverAvalaible = [
		0 => 'redis'
	];

	/** @var  \Redis */

	private static $redis;

}