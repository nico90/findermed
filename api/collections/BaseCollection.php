<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 24/04/17
 * Time: 20:11
 */

namespace api\collections;

class BaseCollection
	extends \Phalcon\Mvc\Micro\Collection
	implements \api\collections\ICollection {

	const MESSAGE_ERROR_COLLECTION_NOT_EXIST	= 'Collection is not defined';

	const DIRECTORY_COLLECTIONS = '\api\collections\%s';

	/**
	 * @param string $collection
	 * @return \Phalcon\Mvc\Micro\Collection
	 * @throws \Exception
	 * @throws \api\exception\ApiException
	 */

	public static function createFromConfig(
		string $collection
	) {
		/**@var \api\collections\ICollection $collection */
		$collection = self::buildDirectoryCollection($collection);

		if( class_exists($collection) ) {
			try {
				$instanceCollection = new $collection();

				return $instanceCollection->getEntity();

			} catch (\Exception $exception) {
				//need notify error for save exception and notify api.
				throw $exception;
			}
		} else {

			throw new \api\exception\ApiException(
				self::MESSAGE_ERROR_COLLECTION_NOT_EXIST
			);
		}
	}

	/**
	 * @param string $collection
	 * @return string
	 */
	private static function buildDirectoryCollection(
		string $collection
	) : string {
		return sprintf(
			self::DIRECTORY_COLLECTIONS,
			$collection
		);
	}
}