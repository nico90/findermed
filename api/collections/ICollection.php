<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 07/02/17
 * Time: 14:53
 */

namespace api\collections;


use Phalcon\Mvc\Micro\Collection;

interface ICollection
{
    const ENDPOINT_ACTION_POST		= "post";
    const ENDPOINT_ACTION_PUT		= "put";
    const ENDPOINT_ACTION_GET		= "get";
    const ENDPOINT_ACTION_DELETE	= "delete";

    public static function createFromConfig(string $collection);
}