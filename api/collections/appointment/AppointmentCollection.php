<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 17/11/16
 * Time: 11:39
 */

namespace api\collections\appointment;

class AppointmentCollection
	extends \api\collections\BaseCollection {

	const ENDPOINT_APPOINTMENT	= "/appointment";

	const ENTITY_APPOINTMENT                =
		\api\controller\appointment\AppointmentController::class;

    public function getEntity() {

		$user = new \Phalcon\Mvc\Micro\Collection();

		// Define routes
		$user->setHandler(
			\api\controller\FactoryController::getControllerString(
				static::ENTITY_APPOINTMENT
			),true
		);

//		$user->get(self::ENDPOINT_APPOINTMENT, static::ENDPOINT_ACTION_GET);

		$user->post(self::ENDPOINT_APPOINTMENT, static::ENDPOINT_ACTION_POST);

        return $user;

    }
}
