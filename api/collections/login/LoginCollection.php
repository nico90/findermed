<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 17/11/16
 * Time: 11:39
 */

namespace api\collections\login;

class LoginCollection
	extends \api\collections\BaseCollection {

    const ENDPOINT_LOGIN	= "/login";

	const ENTITY                = \api\controller\login\LoginController::class;

    public function getEntity() {

        $login = new \Phalcon\Mvc\Micro\Collection();

		// Define routes
		$login->setHandler(
			\api\controller\FactoryController::getControllerString(static::ENTITY),
			true
		);

		$login->post(self::ENDPOINT_LOGIN, static::ENDPOINT_ACTION_POST);

        return $login;

    }
}
