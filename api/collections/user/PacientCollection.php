<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 17/11/16
 * Time: 11:39
 */

namespace api\collections\user;

class PacientCollection
	extends \api\collections\BaseCollection {

	const ENDPOINT_PACIENT	= "/pacient";
	const ENDPOINT_USER_ME  = "/pacient/me";
	const ENDPOINT_PACIENT_ACTIVATE  = "/pacient/activate/{uid:[1-9][0-9]*}";

	const ENTITY_USER                =
		\api\controller\user\pacient\PacientController::class;

    public function getEntity() {

		$user = new \Phalcon\Mvc\Micro\Collection();

		// Define routes
		$user->setHandler(
			\api\controller\FactoryController::getControllerString(static::ENTITY_USER),true
		);

		$user->get(self::ENDPOINT_USER_ME, static::ENDPOINT_ACTION_GET);
		$user->post(self::ENDPOINT_PACIENT, static::ENDPOINT_ACTION_POST);
		$user->put(self::ENDPOINT_PACIENT_ACTIVATE, static::ENDPOINT_ACTION_PUT);

        return $user;

    }
}

