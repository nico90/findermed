<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 17/11/16
 * Time: 11:39
 */

namespace api\collections\user;

class ProfessionalCollection
	extends \api\collections\BaseCollection {

	const ENDPOINT_PROFESSIONAL	= "/professional";
	const ENDPOINT_USER_ME      = "/professional/me";

	const ENTITY_USER                =
		\api\controller\user\professional\ProfessionalController::class;

    public function getEntity() {

		$user = new \Phalcon\Mvc\Micro\Collection();

		// Define routes
		$user->setHandler(
			\api\controller\FactoryController::getControllerString(static::ENTITY_USER),true
		);
		$user->get(self::ENDPOINT_USER_ME, static::ENDPOINT_ACTION_GET);

		$user->post(self::ENDPOINT_PROFESSIONAL, static::ENDPOINT_ACTION_POST);

        return $user;

    }
}
