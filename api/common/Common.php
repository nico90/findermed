<?php
/**
 * Created by IntelliJ IDEA.
 * User: nicoksq
 * Date: 10/10/17
 * Time: 14:57
 */

namespace common;


class Common {

	/**
	 * Convert words with underscore to camelCase
	 *
	 * @param string $value
	 * @return string
	 */
	public static function toCamelCase(string $value) : string {

		$value = ucwords(str_replace(array('-', '_'), ' ', $value));
		$value = str_replace(' ', '', $value);

		return lcfirst($value);
	}

	/**
	 * parseTimeRange
	 * parses a time range in the form of
	 * '08:55-22:00'
	 * @param $timeRange 'hh:mm-hh:mm' '08:55-22:00'
	 * @return mixed ['hourStart'=>, 'minuteStart'=>, 'hourEnd'=>, 'minuteEnd'=>]
	 */
	public static function parseTimeRange($timeRange)	{
		// no validating just parsing
		preg_match(
			'/(\d{1,2}:\d{2})-(\d{1,2}:\d{2})/',
			$timeRange,
			$matches
		);

		$start = new \DateTime( $matches[1]);
		$end	= new \DateTime( $matches[2]);
		$time['start']	= $start->format('H:i');
		$time['end']	= $end->format('H:i');


		return $time;
	}
}