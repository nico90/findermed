<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 27/04/17
 * Time: 10:49
 */

namespace api\common;


class Config {


	static private $_instance;
	private $_data;

	static public function getInstance($file = 'config.ini')
	{
		if (!isset(self::$_instance[$file])) {
			self::$_instance[$file] = new \Phalcon\Config\Adapter\Ini($file);
		}

		return self::$_instance[$file];
	}

	private function __construct($file)
	{
		$this->_data = parse_ini_file($file, true);
	}

	public function getByKey($key)
	{
		foreach ($this->getSections() as $section) {
			$section = $this->getSection($section);
			if (in_array($key, $section)) {
				return $section[$key];
			}
		}
	}

	public function getBySection($section, $key)
	{
		if (isset($this->_data[$section][$key])) {
			return $this->_data[$section][$key];
		}
	}

	public function getSection($section)
	{
		if (isset($this->_data[$section])) {
			return $this->_data[$section];
		}
	}

	public function getSections()
	{
		if (is_array($this->_data)) {
			return array_keys($this->_data);
		}
	}

}