<?php

namespace common;


/**
 * Abstract class to inherit from when an enum has to be implemented
 *
 * @abstract
 * @author Nicolas Degiorgis
 *
 * @version 1.1 (Mauro Titimoli)
 * 		- Now Enum implements \common\IRepresentable
 *
 * @version 1.2 (Mauro Titimoli)
 * 		- Moved to common namespace
 *
 * @version 1.3 (Mauro Titimoli)
 * 		- Added Enum::CLASS_NAME
 * 		- Added Enum::ensureId and Enum::ensureName methods
 * 		- Improved exceptions messages
 */
abstract class Enum
		implements
			IEnum,
			\common\IRepresentable {

	const CLASS_NAME = __CLASS__;

	const ERROR_INVALID_ENUM_NAME       = 1;
	const ERROR_INVALID_ENUM_ID         = 2;
	const ERROR_INVALID_ENUM_DEFINITION = 3;
	const ERROR_MISSING_ENUM_DEFINITION = 4;

	const PROPERTY_ID   = 'id';
	const PROPERTY_NAME = 'name';

	const REPRESENTATION_NAME = self::PROPERTY_NAME;

	/**
	 * @static
	 * @access public
	 *
	 * @param int $id
	 *
	 * @throws \InvalidArgumentException
	 * 		- ERROR_INVALID_ENUM_NAME
	 * 		  - If name is not string
	 * 		  - If name is not in CURRENT_CLASS::$id*
	 */
	static public function ensureId($id) {

		if (! is_int($id)) {
			throw new \InvalidArgumentException(
				'Expected id to be int, ' .
				'but instead got: ' . gettype($id),
				self::ERROR_INVALID_ENUM_ID
			);
		}

		$metaData = self::getMetaData();

		if (! isset($metaData->enumInv[$id])) {
			throw new \InvalidArgumentException(
				'Expected id to be in ' .
				get_called_class() . '::(' .
				implode(', ', array_keys($metaData->enumInv)) .
				"), but instead got: {$id}",
				self::ERROR_INVALID_ENUM_ID
			);
		}
	}

	/**
	 * @static
	 * @access public
	 *
	 * @param string	$name
	 *
	 * @throws \InvalidArgumentException
	 * 		- ERROR_INVALID_ENUM_NAME
	 * 		  - If name is not string
	 * 		  - If name is not in CURRENT_CLASS::$name*
	 */
	static public function ensureName($name) {

		if (! is_string($name)) {
			throw new \InvalidArgumentException(
				'Expected name to be string, ' .
				'but instead got: ' . gettype($name),
				self::ERROR_INVALID_ENUM_NAME
			);
		}

		$metaData = self::getMetaData();

		if (! isset($metaData->enum[$name])) {
			throw new \InvalidArgumentException(
				'Expected name to be in ' .
				get_called_class() . '::(' .
				implode(', ', array_keys($metaData->enum)) .
				"), but instead got: {$name}",
				self::ERROR_INVALID_ENUM_ID
			);
		}
	}

	/**
	 * @param array      $representation
	 * @param array|null $params
	 *
	 * @return static
	 */
	static public function fromRepresentation(
		array $representation,
		array $params = null
	) {

		if (static::REPRESENTATION_NAME === static::PROPERTY_ID) {
			return static::getInstanceById(@ $representation[static::PROPERTY_ID]);
		}

		return static::getInstanceByName(@ $representation[static::PROPERTY_NAME]);
	}

	/**
	 * get id of enum
	 *
	 * @static
	 * @access public
	 *
	 * @param string	$name
	 *
	 * @return int
	 */
	static public function getIdOf($name) {

		static::ensureName($name);

		$metaData = self::getMetaData();

		return $metaData->enum[$name];
	}

	/**
	 * @static
	 * @access public
	 *
	 * @param int	$id
	 *
	 * @return static
	 * @return self
	 */
	static public function getInstanceById($id) {

		static::ensureId($id);

		return self::doGetInstanceById($id);
	}

	/**
	 * @static
	 * @access public
	 *
	 * @param string	$name
	 *
	 * @return static
	 * @return self
	 */
	static public function getInstanceByName($name) {

		$id = static::getIdOf($name);

		return self::doGetInstanceById($id);
	}

	/**
	 * get name of enum
	 *
	 * @static
	 * @access public
	 *
	 * @param int	$id
	 *
	 * @return string
	 */
	static public function getNameOf($id) {

		static::ensureId($id);

		$metaData = self::getMetaData();

		return $metaData->enumInv[$id];
	}

	/**
	 * @see \common\IRepresentable::getRepresentation
	 */
	public function getRepresentation(
		$validate = \common\IRepresentable::DEFAULT_REPRESENTATION_VALIDATE
	) {
		if (static::REPRESENTATION_NAME === static::PROPERTY_ID) {
			return array(
				static::PROPERTY_ID => $this->getId()
			);
		}

		return array(
			static::PROPERTY_NAME => $this->getName()
		);
	}

	/**
	 * get id
	 *
	 * @access public
	 *
	 * @return int
	 */
	public function getId() {

		return $this->id;
	}

	/**
	 * get name
	 *
	 * @access public
	 *
	 * @return string
	 */
	public function getName() {

		return $this->name;
	}

	/**
	 * @static
	 * @access private
	 *
	 * @param int	$id
	 *
	 * @return static
	 * @return self
	 */
	static private function doGetInstanceById($id) {

		$metaData = self::getMetaData();

		if (! isset($metaData->instance[$id])) {
			$metaData->instance[$id] = new static($id, $metaData->enumInv[$id]);
		}

		return $metaData->instance[$id];
	}

	/**
	 * get meta data
	 *
	 * @static
	 * @access private
	 *
	 * @param string	$className
	 *
	 * @return \stdClass
	 */
	static private function getMetaData($className = null) {

		if (! isset($className)) {
			$className = get_called_class();
		}

		if (! isset(self::$metaData[$className])) {
			$enum = static::getEnumDefinition();

			if (! is_array($enum)) {
				throw new \UnexpectedValueException(
					'Expected enum definition to be an array, ' .
					'but instead got: ' . gettype($enum),
					self::ERROR_INVALID_ENUM_DEFINITION
				);
			}

			self::$metaData[$className] = (object) array(
				'enum'      => $enum,
				'enumInv'   => array_flip($enum),
				'instance'  => array()
			);
		}

		return self::$metaData[$className];
	}

	/**
	 * @access private
	 *
	 * @param int   	$id
	 * @param string	$name
	 */
	private function __construct($id, $name) {

		$this->id   = $id;
		$this->name = $name;
	}

	/**
	 * get enum definition
	 * (overload this method for specific implementation)
	 *
	 * @static
	 * @access public
	 *
	 * @return int[]
	 */
	static public function getEnumDefinition() {

		if (! isset(static::$ENUM)) {
			throw new \RuntimeException(
				'Expected ' . get_called_class() . '::$ENUM to be defined',
				self::ERROR_MISSING_ENUM_DEFINITION
			);
		}

		return static::$ENUM;
	}

	/**
	 * meta data for store information about enum classes
	 *
	 * @static
	 * @access private
	 *
	 * @var array
	 */
	static private $metaData;

	/**
	 * @access private
	 *
	 * @var int
	 */
	private $id;

	/**
	 * @access private
	 *
	 * @var string
	 */
	private $name;


	//REMOVE THIS!!!
	protected function setId($id) {

		$this->id = $id;
	}

	protected function setName($name) {

		$this->name = $name;
	}
}

class_alias('common\Enum', 'Enum');
