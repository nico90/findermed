<?php

namespace common;


/**
 * @author Nicolas Degiorgis
 *
 * @version 1.1 (Mauro Titimoli)
 * 		- Moved to common namespace
 */
interface IEnum {

	/**
	 * @access public
	 *
	 * @return int
	 */
	public function getId();

	/**
	 * @access public
	 *
	 * @return string
	 */
	public function getName();
}
