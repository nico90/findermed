<?php

namespace common;

interface IRepresentable {

	const DEFAULT_REPRESENTATION_VALIDATE = true;

	/**
	 * Returns an instance of the specific class where
	 * this interface is implemented
	 *
	 * @param array	$representation
	 * @param array	$params = null
	 *
	 * @return self
	 */
	static public function fromRepresentation(
		array $representation,
		array $params = null
	);

	/**
	 * Returns a representation (array) of current instance.
	 * Returned value could be then used as input of
	 * IRepresentable::fromRepresentation to obtain current instance
	 *
	 * @access public
	 *
	 * @param boolean	$validate = \common\IRepresentable::DEFAULT_REPRESENTATION_VALIDATE
	 * 		It can be used to control instance mandatory properties or state
	 *
	 * @return array
	 */
	public function getRepresentation(
		$validate = \common\IRepresentable::DEFAULT_REPRESENTATION_VALIDATE
	);
}
