<?php
namespace api\common;
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 24/04/17
 * Time: 15:21
 *
 * Here proccess errors, save in database and return build json.
 * @TODO: change response message to column database
 */
class NotifyError
	extends \Phalcon\Mvc\Model {

    const TABLE_DEFAULT 		= 'errors_log';
    const KEY_RESPONSE_MESSAGE	= 'error';

	public function initialize() {
		$this->setSource(self::TABLE_DEFAULT);

	}

	/**
	 * @param string $error
	 */
	public static function sendErrorToApi(
		string $error
	){
		if(!empty($error)) {
			self::setError($error);
			self::save();
		}

		die($error);
	}

	/**
	 * @return string
	 */
	public function getError() : string
	{
		return $this->error;
	}

	/**
	 * @param string $error
	 */
	public function setError(string $error) {
		$this->error = $error;
	}

	/**
	 * @var string $error
	*/
	private $error;

}