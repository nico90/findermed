<?php

namespace common;


/**
 * @author Nicolas Andreoli
 */
trait ValueObjectTrait {

	/**
	 * @access public
	 *
	 * @param array	$properties = null
	 * @param bool 	$setAlways  = true
	 *
	 * @return ValueObjectTrait
	 */
	public function onConstruct(array $properties=null, $setAlways=false) {

		$propertyNames = $this->getPropertyNames();

		if ($setAlways) {
			foreach ($propertyNames as $propName) {
				$propValue = (isset($properties[$propName])) ?
					$properties[$propName] :
					null;

				$this->set($propName, $propValue);
			}
		}
		elseif (isset($properties)) {
			foreach ($propertyNames as $propName) {
				if (array_key_exists($propName, $properties)) {
					$this->set($propName, $properties[$propName]);
				}
			}
		}
	}

	/**
	 * @access public
	 *
	 * @param string	$propName
	 */
	public function get($propName) {

		$propName = ucfirst($propName);

		$getter = 'get' . $propName;

		if (! method_exists($this, $getter)) {
			$getter = 'is' . $propName;

			if (! method_exists($this, $getter)) {
				$getter = 'has' . $propName;
			}
		}

		return $this->$getter();
	}

	/**
	 * @access public
	 *
	 * @return array
	 */
	public function getProperties() {

		$propertyNames = $this->getPropertyNames();

		$properties = array();

		foreach ($propertyNames as $propName) {
			$properties[$propName] = $this->get($propName);
		}

		return $properties;
	}

	/**
	 * @access public
	 *
	 * @return \ReflectionClass
	 */
	public function getReflection() {

		static $reflection;

		if (! isset($reflection)) {
			$reflection = new \ReflectionClass(
				get_class($this)
			);
		}

		return $reflection;
	}

	/**
	 * @access public
	 *
	 * @param string	$propName
	 * @param mixed 	$value
	 */
	public function set($propName, $value) {

		$setter =
			'set' .
			Common::toCamelCase(ucfirst($propName));

		$this->$setter($value);
	}

	/**
	 * @access public
	 *
	 * @param array $properties
	 *
	 * @return self
	 */
	public function setProperties(array $properties) {

		foreach ($properties as $propName => $value) {
			$this->set($propName, $value);
		}

		return $this;
	}

	/**
	 * @access protected
	 *
	 * @return array
	 */
	protected function getPropertyNames() {

		static $propertyNames;

		if (! isset($propertyNames)) {
			$reflection = $this->getReflection();

			$constants = $reflection->getConstants();

			$propertyNames = array();

			foreach ($constants as $name => $value) {
				// detect if constant define a property
				if (strpos($name, 'PROPERTY_') === 0) {
					$propertyNames[] = $value;
				}
			}
		}

		return $propertyNames;
	}
}