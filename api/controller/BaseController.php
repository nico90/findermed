<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 27/04/17
 * Time: 15:51
 */

namespace api\controller;

abstract class BaseController
	extends \Phalcon\Mvc\Controller {

	const REQUEST_METHOD_GET	=	'GET';
	const REQUEST_METHOD_POST	=	'POST';
	const REQUEST_METHOD_PUT	=	'PUT';

	const ACTION	= 'action';
	const ARGS		= 'args';
	const TOKEN		= 'token';

	const CREATE_METHOD_SET_FORMAT		= 'set%s';
	const CREATE_METHOD_ACTION_FORMAT	= 'action%s';

	const RESPONSE_STATUS			= 'status';
	const RESPONSE_MESSAGES			= 'messages';

	const NEED_TOKEN	= true;

	const HEADER_SECURITY	= 'Token-Security';

	const KEY_DATA			= 'data';

	protected function onConstruct() {

		/** @var  \Phalcon\Http\Request $this->request*/
		$this->request = new \Phalcon\Http\Request();

		self::$actionNeedToken =
			array_merge(self::$actionNeedToken, static::$actionNeedToken);

		//@todo change if ifelse for switch

		if($this->request->getMethod() == self::REQUEST_METHOD_POST) {

			$request 		= $this->request->getJsonRawBody(true);

			$this->args		= $request[self::ARGS];
			$this->action	= $request[self::ACTION];

			if(array_key_exists(self::TOKEN, $request)) {
				$this->token	= $request[self::TOKEN];
			}

			if(empty($this->args) || empty($this->action)) {
				throw new \api\exception\ApiException(
					\api\Api::HTTP_MESSAGE_INTERNAL_SERVER_ERROR
				);
			}

		}elseif ($this->request->getMethod() == self::REQUEST_METHOD_GET) {

			$headers 		= $this->request->getHeaders();
			if(isset($headers[self::HEADER_SECURITY])){
				$this->token	= $headers[self::HEADER_SECURITY];
			}

			$this->action	= strtolower($this->request->getMethod());

		}elseif ($this->request->getMethod() == self::REQUEST_METHOD_PUT) {

			$uriParts = explode('/', $this->request->getURI());

			$this->action = $uriParts[2];

		}else{
			throw new \api\exception\ApiException(
				\api\Api::HTTP_MESSAGE_NOT_IMPLEMENTED
			);
		}

		if(in_array($this->action, self::$actionNeedToken)) {
			$this->checkToken();
		}

	}

	/**
	 * @param string $action
	 * @return string
	 */
	protected function buildAction(
		string $action
	) : string {
		return sprintf(
			self::CREATE_METHOD_ACTION_FORMAT,
			ucfirst($action)
		);
	}

	/**
	 * @param $jwt
	 * @return object
	 */
	protected function decodeJWT($jwt){
		//@todo here get config key, and dont hardcode
		return \Firebase\JWT\JWT::decode(
			$jwt,
			'OYAwGhVEp7or73ccWSi1KVPXMxKVFs/u1+N1aZabTR/nwIVjYhsOlxCktCw+rtKP4GJiFYFHTTPI17LmC5we9g==',
			["HS256"]
		);
	}

	/**
	 * check token before set.
	 */
	protected function checkToken() {

		if(empty($this->token)) {
			throw new \api\exception\ApiException(
				\api\Api::HTTP_MESSAGE_INTERNAL_SERVER_ERROR
			);
		}

		if(\security\Security::existToken($this->token)) {
			return true;
		}

		$tokenDecode = (array) $this->decodeJWT($this->token);

		if(!empty($tokenDecode)) {
//			if($tokenDecode[self::KEY_DATA]->usertype != static::USER_TYPE) {
//				throw new \api\exception\ApiException(
//					'Action not support for this user'
//				);
//			}

			$this->setUidToken($tokenDecode['uid']);
			$this->setUserTypeToken($tokenDecode[self::KEY_DATA]->usertype);

			return true;
		}

		throw new \api\exception\ApiException(
			\api\Api::HTTP_MESSAGE_BAD_REQUEST,
			\api\Api::HTTP_CODE_BAD_REQUEST
		);

	}

	/**
	 * @return int
	 */
	final public function getUidToken(): int {
		return $this->uidToken;
	}

	/**
	 * @param int $uidToken
	 */
	final public function setUidToken(int $uidToken) {
		$this->uidToken = $uidToken;
	}

	/**
	 * @return user\Type
	 */
	final public function getUserTypeToken(): \api\controller\user\Type {
		return $this->userTypeToken;
	}

	/**
	 * @param int $userTypeToken
	 */
	final public function setUserTypeToken(int $userTypeToken) {
		$this->userTypeToken =
			\api\controller\user\Type::getInstanceById($userTypeToken);
	}

	protected function getUsernameByToken() {
		$tokenDecode = (array) $this->decodeJWT($this->token);

		$data =
			$tokenDecode
			[self::KEY_DATA];

		return !empty($data->username) ?
			$data->username :
			false;
	}

	/** @var  string */
	protected $action;

	/** @var  array */
	protected $args;

	protected $model;

	/** @var \Phalcon\Http\Request $this->request */
	protected $request;

	/** @var  string */
	protected $token;

	/** @var  string */
	protected $appToken;

	/** @var int*/
	private $uidToken;

	/** @var \api\controller\user\Type */
	private $userTypeToken;

	/**
	 * Add here when need request token for action
	 * */

	protected static $actionNeedToken	= array(
		0 => 'get',
		1 => 'update'
	);

}