<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 07/02/17
 * Time: 14:32
 */

namespace api\controller;

use api\exception\ApiException;

class FactoryController
    extends \api\Factory
{

	/**
	 * @param string $entity
	 * @param array|null $args
	 * @return BaseController
	 * @throws ApiException
	 */
    public static function onCreate(
    	string $entity,
		array $args = null
	){
        if(class_exists($entity)) {
            $entityController =  "\\".$entity;
            return new $entityController($args);
        }else{
            throw new ApiException('Controller not exist');
        }
    }

	/**
	 * @param string $entity
	 * @return string
	 * @throws ApiException
	 * */
	public static function getControllerString(string $entity){
		if(class_exists($entity)) {
			$entityController =  "\\".$entity;
			return $entityController;
		}else{
			throw new ApiException('Controller not exist');
		}
	}

}