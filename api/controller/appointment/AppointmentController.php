<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\controller\appointment;


class AppointmentController
    extends \api\controller\BaseController {

    const MODEL = \api\entitys\appointment\Appointment::class;

    const RESPONSE_APPOINTMENTS = 'appointments';

    const ARG_KEY_BETWEEN		= 'between';
    const ARG_KEY_BETWEEN_FROM	= 'from';
    const ARG_KEY_BETWEEN_TO	= 'to';
    const ARG_KEY_STATUS		= 'status';

    public function onConstruct() {
		parent::onConstruct();

		$this->model	= \api\entitys\FactoryModel::onCreate(self::MODEL);

    }
    /**
     * @return array
     * @throws \api\exception\EntityException
     */
    public function post() : array {
		$this->ensureRequest();
		$this->ensureAction();

		return $this->action();

    }

	/**
	 * @return array
	 * @throws \api\exception\ApiException
	 */
	protected function action()  : array {

		$method = $this->buildAction($this->action);
		if( method_exists($this, $method) ) {
			return $this->$method();
		}

		throw new \api\exception\ApiException('action is not avalaible');

	}

	/**
	 * @return array
	 * @throws \api\exception\ApiException
	 *
		"args" : {
			"dateAppointmentStart":"2018-03-23 11:1:00",
			"typeQueryId":2,
			"professionalId":1,
			"clinicId":1
		}
	 *
	 */
	private function actionCreate() : array {

		$messages = [];

		if(!empty($this->getUidToken())) {
			$this->model->setUid(
				$this->getUidToken()
			);
		}

		foreach($this->args as $property => $value) {
			if(in_array($property, self::$argsCreate)) {

				$methodName = sprintf(
					self::CREATE_METHOD_SET_FORMAT,
					ucfirst($property)
				);

				$methodName = \common\Common::toCamelCase($methodName);

				$this
					->model
					->$methodName($value);
			}
		}

		$now = new \DateTime(
			null,
			new \DateTimeZone('UTC')
		);

		if($this->model->getDateAppointmentStart() < $now) {
			throw new \api\exception\ApiException(
				'Incorrect date'
			);
		}

		if(!empty($this->model->getDateAppointmentStart())) {

			$endtime = $this->model->getDateAppointmentStart();

			$timeQueryInAppointment = \api\entitys\typeQuery\TypeQuery::getById(
				$this->model->getTypeQueryId()
			);

			$lifeAppointment = date('i', strtotime($timeQueryInAppointment->getDuration()));

			$endtime->modify('+' . $lifeAppointment.  'minutes');
			$this->model->setDateAppointmentEnd($endtime->format('Y-m-d H:i:s'));

		}

		$this->ensure();

		$resultAppointment = $this->model->save();

		if(!empty($this->model->getMessages())) {
			foreach ($this->model->getMessages() as $message) {
				$messages[] = $message->getMessage();
			}
		}

		$response =  array(
			static::RESPONSE_STATUS		=> $resultAppointment,
			static::RESPONSE_MESSAGES	=> $messages
		);

		return $response;

	}

	/**
	 * @return array
	 * @throws \api\exception\ApiException
	 *
		"args" : {
			"id":1, #appointment id
			"status":3
		}
	 *
	 */

	private function actionChange() : array {

		$messages = [];

		$this->model =
			\api\entitys\appointment\Appointment::getByIdAndPid(
				$this->args[\api\entitys\appointment\Appointment::PROPERTY_ID],
				$this->getUidToken()
			);

		$this->model->setStatus(
			$this->args[\api\entitys\appointment\Appointment::PROPERTY_STATUS]
		);

		$resultAppointment = $this->model->save();

		if(!empty($this->model->getMessages())) {
			foreach ($this->model->getMessages() as $message) {
				$messages[] = $message->getMessage();
			}
		}

		$response =  array(
			static::RESPONSE_STATUS		=> $resultAppointment,
			static::RESPONSE_MESSAGES	=> $messages
		);

		return $response;
	}

	/**
	 * @return array
	 * @throws \api\exception\ApiException
	 *
		"args" : {
			"between":{
				"from":"2018-01-01",
				"to":"2018-03-23"
			},
			"status":1
		}
	 *
	 */

	private function actionGet() : array {

		$messages = [];

		$userType = $this->getUserTypeToken();

		$user =
			\api\controller\user\UserController::createInstanceByType(
				$userType,
				$this->getUidToken()
			);

		$this->ensureDate();

		if(!empty($this->model->getMessages())) {
			foreach ($this->model->getMessages() as $message) {
				$messages[] = $message->getMessage();
			}
		}

		$appointments =
			$user->appointments->filter(function($appointment){

			$status = true;

			if(!empty($this->args[self::ARG_KEY_STATUS])) {
				$status =
					$appointment->getStatus()->getId() ===
					$this->args[self::ARG_KEY_STATUS];
			}

			if(
				$this->args[self::ARG_KEY_BETWEEN][self::ARG_KEY_BETWEEN_FROM]->format('Y-m-d') <= $appointment->getDateAppointmentStart()->format('Y-m-d') &&

				$this->args[self::ARG_KEY_BETWEEN][self::ARG_KEY_BETWEEN_TO]->format('Y-m-d') >= $appointment->getDateAppointmentStart()->format('Y-m-d') &&
				$status
			){
				return $appointment;
			}
		});

		$response =  array(
			static::RESPONSE_APPOINTMENTS	=> $appointments,
			static::RESPONSE_MESSAGES		=> $messages
		);

		return $response;
	}

	private function ensureDate() {
		$this->args[self::ARG_KEY_BETWEEN][self::ARG_KEY_BETWEEN_TO] =
			new \DateTime($this->args[self::ARG_KEY_BETWEEN][self::ARG_KEY_BETWEEN_TO]);
		$this->args[self::ARG_KEY_BETWEEN][self::ARG_KEY_BETWEEN_FROM] =
			new \DateTime($this->args[self::ARG_KEY_BETWEEN][self::ARG_KEY_BETWEEN_FROM]);
	}

	/**
	 * @throws \api\exception\ApiException
	 */
	private function ensure() {

		if($this->checkTimeZone()) {
			throw new \api\exception\ApiException('Can not posible take the appointment');
		}

		if($this->checkRelaxZone()) {
			throw new \api\exception\ApiException('The Professional will take the holidays in this date');
		}

		if($this->checkAppointmentIsOcupated()) {
			throw new \api\exception\ApiException('The Appointment was reservated.');

		}

	}

	/**
	 * check if professional teka a vacation
	 *
	 * @return bool
	 */
	private function checkRelaxZone() {

		$professional =
			\api\entitys\user\professional\Professional::getById(
				$this->model->getProfessionalId()
			);

		$isVacationTime = $professional->vacationTimeZone->filter(function ($vacation) {

			if(
				$this->model->getDateAppointmentStart()->format('Y-m-d') > $vacation->getStart() &&
				$this->model->getDateAppointmentStart()->format('Y-m-d') < $vacation->getEnd()
			) {
				return $vacation;
			}

		});

		return !empty($isVacationTime);

	}

	/**
	 * check if exist timezone for this professional
	 *
	 * @return bool
	 */
	private function checkTimeZone() {

		$professional =
			\api\entitys\user\professional\Professional::getById(
				$this->model->getProfessionalId()
			);

		$isHourAvalaible =
			$professional->workTimeZone->filter(function ($workTimeHour) {
			/** @var  \api\entitys\user\professional\ProfessionalHourZones $workTimeHour  */
			$start	=  new \DateTime( $workTimeHour->getHourStart() );
			$end 	=  new \DateTime( $workTimeHour->getHourEnd() );

			$break	= \common\Common::parseTimeRange($workTimeHour->getBreakTime());

			$dayAppointment = \api\controller\user\Days::getIdOf(
				$this->model->getDateAppointmentStart()->format('l')
			);

			$isInBreak = $this->model->getDateAppointmentStart()->format('H:i') > $break['start'] &&
				$this->model->getDateAppointmentStart()->format('H:i') < $break['end'];

			$isWorkZone =
				$this->model->getDateAppointmentStart()->format('H:i') > $start->format('H:i') &&
				$this->model->getDateAppointmentStart()->format('H:i') < $end->format('H:i');

			$isAppointmentFinishPostWork = $this->model->getDateAppointmentEnd()->format('H:i') > $end->format('H:i');

			$isAppointmentFinishPostStartBreak = $this->model->getDateAppointmentEnd()->format('H:i') > $break['start'];

			if(
				$workTimeHour->getClinicId() === $this->args['clinicId'] &&
				$dayAppointment === $workTimeHour->getDay()
			) {
				//si estoy aca, es porque hay clinica y medico en ese dia.
				if(!$isInBreak && $isWorkZone){
					//si estoy en el break no entro
					//si estoy en horario de trabajo entro.
					if(!$isAppointmentFinishPostWork && !$isAppointmentFinishPostStartBreak) {
						return $workTimeHour;
					}
				}

				//chekeo que el turno no este ocupado
			}
		});

		//si esta vacio es error.
		return empty($isHourAvalaible);

	}

	/**
	 *
	 * check if appointment was ocupated
	 * @return bool
	 */
	private function checkAppointmentIsOcupated() {

		$appointments = $this->model->checkIfAppointmentIsAvalaible(
			$this->model->getProfessionalId(),
			$this->model->getDateAppointmentStart(),
			\api\controller\appointment\AppointmentStatusType::getIdOf(
				\api\controller\appointment\AppointmentStatusType::PENDING
			)
		);

		$result = $appointments->filter(function ($child) {

			if(
				!(
					$this->model->getDateAppointmentStart()->format('H:i') >= $child->getDateAppointmentStart()->format('H:i') &&
					$this->model->getDateAppointmentStart()->format('H:i') < $child->getDateAppointmentEnd()->format('H:i') ||
					$this->model->getDateAppointmentEnd()->format('H:i') <= $child->getDateAppointmentStart()->format('H:i') &&
					$this->model->getDateAppointmentEnd()->format('H:i') > $child->getDateAppointmentEnd()->format('H:i')
				)
			) {
				return $child;
			}

		});

		//Sino llega a filtrar ningun resultado, significa que el turno esta disponible, y devuelvo true

		return count($result) !== count($appointments->toArray());

	}

	/**
	 * @throws \api\exception\ApiException
	 */
	private function ensureRequest() {
		if(empty($this->args)) {
			throw new \api\exception\ApiException(
				'Args can\'t be null'
			);
		};
	}

	/**
	 * @return bool
	 * @throws \api\exception\ApiException
	 */
	private function ensureAction() : bool {

		if (!in_array(
			$this->action,
			self::$actionsAvalaible
		)) {
			throw new \api\exception\ApiException(
				'Action not supported'
			);
		}

		return true;
	}

	protected static $argsCreate = array(
		"dateAppointmentStart",
		"typeQueryId",
		"professionalId",
		"clinicId"
	);

	protected static $actionsAvalaible = array(
		1 => 'create',
		2 => 'change',
		3 => 'get'
	);

	/**
	 * Add here when need request token for action
	 * */

	protected static $actionNeedToken	= array(
		0 => 'create',
		1 => 'change',
		2 => 'get'
	);

	/** @var \api\entitys\appointment\Appointment $model */
	protected $model;

}