<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 07/09/17
 * Time: 11:20
 */

namespace api\controller\appointment;

class AppointmentStatusType
	extends \common\Enum {

	const PENDING		= 'pending';
	const IN_PROGRESS	= 'inProgress';
	const FINALIZED		= 'finalized';
	const CANCELLED		= 'cancelled';


	/**
	 * @return array
	 */
	public static function getEnum() {

		return self::$ENUM;
	}

	/**
	 * @var array
	 */
	static protected $ENUM = array(
		self::PENDING		=>	1,
		self::IN_PROGRESS	=>	2,
		self::FINALIZED		=> 	3,
		self::CANCELLED		=> 	4
	);

}