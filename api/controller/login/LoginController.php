<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\controller\login;


/**
 * Action avalaible:
 * 	Login: check in db, save in redis, return token
 *
 */

class LoginController
    extends \api\controller\BaseController {

    const MODEL = \api\entitys\user\User::class;

    const ERROR_GET_USER = 'Param id is mandatory';

    const ARG_KEY_CREDENTIAL = 'credential';

	const REDIS_KEY_LOGIN = 'fd:s.%s';

	const NEED_TOKEN	= false;

    public function onConstruct() {
		parent::onConstruct();
		$this->model	= \api\entitys\FactoryModel::onCreate(self::MODEL);
    }

    /**
     * @return array
     * @throws \api\exception\EntityException
     */
    public function post() : array {
		$this->ensureRequest();
		$this->ensureAction();

		return $this->action();

    }

	/**
	 * @return array
	 * @throws \api\exception\ApiException
	 */
	protected function action()  : array {

		$method = $this->buildAction($this->action);
		if( method_exists($this, $method) ) {
			return $this->$method();
		}

		throw new \api\exception\ApiException('action is not avalaible');

	}

	/**
	 * @return array
	 * @throws \api\exception\ApiException
	 */
	private function actionLogin() : array {

		$credential =
			new \auth\Credential(
				$this->args[self::ARG_KEY_CREDENTIAL]
			);

		try{

			/** @var \api\entitys\user\User  */
			$user = $this->model->findForLogin(
				$credential
			);

			if(!empty($user)) {
				$session = \auth\Session::getInstance();

				/** @var \api\entitys\user\User $user */

				$userData = [
					'id'		=> $user->getId(),
					'username'	=> $user->getUsername(),
					'name'		=> $user->getName(),
					'email'		=> $user->getEmail(),
					'phone'		=> $user->getPhone(),
					'usertype'	=> $user->getUsertype()->getId(),
				];

				$session = $session->createSession($userData);

				return [
						"uid"			=> $user->getId(),
                        "token"			=> $session->token,
                        "sessionToken"	=> $session->sessionToken,
                        "deviceId"		=> null
				];

			}

		} catch (\api\exception\ApiException $e) {
			throw new \api\exception\ApiException($e->getMessage());
		}

	}

	/**
	 * @throws \api\exception\ApiException
	 */
	private function ensureRequest() {
		if(empty($this->args)) {
			throw new \api\exception\ApiException(
				'Args can\'t be null'
			);
		};
	}

	/**
	 * @return bool
	 */
	private function ensureAction() : bool {

		if (!in_array(
			$this->action,
			self::$actionsAvalaible
		)) {
			throw new \api\exception\ApiException(
				'Action not supported'
			);
		}

		return true;
	}

	protected static $actionsAvalaible = array(
		1 => 'login'
	);

}