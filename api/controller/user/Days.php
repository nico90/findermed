<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 07/09/17
 * Time: 11:20
 */

namespace api\controller\user;

class Days
	extends \common\Enum {

	const MONDAY		= 'Monday';
	const TUESDAY		= 'Tuesday';
	const WEDNESDAY		= 'Wednesday';
	const THURSDAY		= 'Thursday';
	const FRIDAY		= 'Friday';
	const SATURDAY		= 'Saturday';
	const SUNDAY		= 'Sunday';

	/**
	 * @return array
	 */
	public static function getEnum() {

		return self::$ENUM;
	}

	/**
	 * @var array
	 */
	static protected $ENUM = array(
		self::MONDAY	=>	1,
		self::TUESDAY	=>	2,
		self::WEDNESDAY	=> 	3,
		self::THURSDAY	=> 	4,
		self::FRIDAY	=> 	5,
		self::SATURDAY	=> 	6,
		self::SUNDAY	=> 	7
	);

}