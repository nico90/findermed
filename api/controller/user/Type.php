<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 07/09/17
 * Time: 11:20
 */

namespace api\controller\user;

class Type
	extends \common\Enum {

	const PROFESSIONAL	= 'professional';
	const PACIENT		= 'pacient';
	const ADMIN			= 'admin';

	public function getControllerClass(): string {
		switch ($this->getName()) {
			case self::PROFESSIONAL:
				return \api\entitys\user\professional\Professional::class;
				break;
			case self::PACIENT:
				return \api\entitys\user\pacient\Pacient::class;
				break;
			default:
				throw new \Phalcon\Exception('Controller not exist');
		}
	}

	/**
	 * @return array
	 */
	public static function getEnum() {

		return self::$ENUM;
	}

	/**
	 * @var array
	 */
	static protected $ENUM = array(
		self::PROFESSIONAL	=> 0,
		self::PACIENT		=> 1,
		self::ADMIN			=> 2
	);

}