<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\controller\user;


/**
 * Class UserController
 * @package api\controller\user
 *
 */

abstract class UserController
    extends \api\controller\BaseController {

    const MODEL	= \api\entitys\user\User::class;

    const ERROR_GET_USER = 'Param id is mandatory';

    const ARG_KEY_CREDENTIAL	= 'credential';
	const ARG_UID				= 'uid';
	const ARG_TYPE				= 'type';

	const ACTION_GET			= 'get';
	const ACTION_UPDATE			= 'update';
	const ACTION_CREATE			= 'create';
	const ACTION_DELETE			= 'delete';

	const USER_TYPE				= null;

	public function onConstruct() {

		parent::onConstruct();

		$this->model	= \api\entitys\FactoryModel::onCreate(static::MODEL);

		$this->model->setUsertype(static::USER_TYPE);

        $this->driver	= \cache\Driver::createFromConfig();

	}

	/**
	 *
	 * @param int|null $id
	 * @return array|\Phalcon\Mvc\Model
	 * @throws \api\exception\EntityException
	 */
	public function get(int $id = null) {

		$username = $this->getUsernameByToken();
		if(!empty($username)) {
			$user =
				\api\entitys\user\User::getByUsername(
					$username,
					false
				);
			return array_filter((array) $user);

		} else {

			throw new \api\exception\EntityException(
				self::ERROR_GET_USER,
				\api\Api::HTTP_CODE_INTERNAL_SERVER_ERROR
			);
		}

	}

    /**
     * @return array
     * @throws \api\exception\EntityException
     */
    public function post() : array {
		$this->ensureRequest();

		return $this->action();
    }

	/**
	 * @return array
	 * @throws \api\exception\EntityException
	 */
	public function put($param) : array {
		return $this->action($param);
	}

	/**
	 * Este metodo delega los actions.
	 *
	 * @return array
	 * @throws \api\exception\ApiException
	 */
	protected function action($param = null)  : array {
		$method = $this->buildAction($this->action);

		if( method_exists($this, $method) ) {
			return $this->$method($param);
		}

		throw new \api\exception\ApiException('action is not avalaible');

	}

    abstract protected function ensureArgs();

	/**
	 * @return array
	 * @throws \api\exception\EntityException
	 */
	public function delete() : array {
		//@todo change status.
	}

	/**
	 * @param string $username
	 * @return \Phalcon\Mvc\Model | null
	 * @throws \api\exception\EntityException
	 */
	protected function getEntityByUserName(string $username) {

		if(!empty($username)) {

			return \api\entitys\user\User::getByUsername($username);
		} else {

			throw new \api\exception\EntityException(
				self::ERROR_GET_USER,
				\api\Api::HTTP_CODE_INTERNAL_SERVER_ERROR
			);
		}
	}

	/**
	 * @param string $id
	 * @return \Phalcon\Mvc\Model | null
	 * @throws \api\exception\EntityException
	 */
	protected function getEntityById(int $id) {

		if(!empty($id)) {

			return \api\entitys\user\User::getEntityById($id);
		} else {

			throw new \api\exception\EntityException(
				self::ERROR_GET_USER,
				\api\Api::HTTP_CODE_INTERNAL_SERVER_ERROR
			);
		}
	}

	/**
	 * @return array
	 */
	private function actionCreate() {

		$this->ensureArgs();

		$messages = [];

		foreach($this->args as $property => $value) {
			$methodName = sprintf(
				self::CREATE_METHOD_SET_FORMAT,
				ucfirst($property)
			);

			$methodName = \common\Common::toCamelCase($methodName);

			$this
				->model
				->$methodName($value);
		}

		$resultUser = $this->model->save();

		if(
			method_exists($this, 'setHashActivate') &&
			$resultUser
		) {
			$this->setHashActivate($this->model->getId());
		}

		if(!empty($this->model->getMessages())) {
			foreach ($this->model->getMessages() as $message) {
				$messages[] = $message->getMessage();
			}
		}

		$response =  array(
			self::RESPONSE_STATUS	=> $resultUser,
			self::RESPONSE_MESSAGES	=> $messages
		);

		return $response;

	}

	/**
	 * Request for update
	 *
	 * {
		"action": "update",
		"args" : {
			"username" : "idadone2", -> Mandatory
			"password" : "palmaspalm",
			"lastname" : "Andreoli",
			"name" : "irina",
			"du" : 30908081,
			"phone" : 113027027,
			"email" : "idadone@hotmail.com",
			"birthdate" : "05-05-1980"
	 	},
		"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTA4NDMwMTEsImp0aSI6Inl3UCs4MWVEd1dZYjZkMFY2cEUrbnNZNDFZXC9JVGdUS2xHUDdoUEVETDNVPSIsImlzcyI6ImFwaS5maW5kZXJtZWQuY29tIiwiZXhwIjoxNTEwODQ2NjExLCJ1aWQiOjMwLCJkYXRhIjp7ImlkIjozMCwidXNlcm5hbWUiOiJpZGFkb25lMiIsIm5hbWUiOiJpcmkiLCJlbWFpbCI6ImlkYWRvbmVAaG90bWFpbC5jb20iLCJiaXJ0aGRhdGUiOiIxOTgwLTA1LTA1IDAwOjAwOjAwIn19.jdxzoj4a8B_vnY34a1DlCmUgP-JwmgaL_UNDE9b5x9s"
	}
	*/

	private function actionUpdate() {

		$user = $this->model->getEntityByUsername(
			$this->args[\api\entitys\user\User::PROPERTY_USERNAME]
		);

		if(empty($user)) {
			throw new \api\exception\ApiException('Ups, i have the problem.');
		}

		$username = $this->getUsernameByToken();

		$usernameSession = $this->driver->get(
			$this->buildKeySession(),
			[\api\entitys\user\User::PROPERTY_USERNAME]
		);

		if(
			$username !==
			$usernameSession[\api\entitys\user\User::PROPERTY_USERNAME] &&
			$username !==
			$this->args[\api\entitys\user\User::PROPERTY_USERNAME]
		) {
			throw new \api\exception\ApiException('Session not coincidence with user');
		}

		foreach($this->args as $property => $value) {

			if(
				$this->action == self::ACTION_UPDATE &&
				in_array($property, self::$propertyUpdateNotAvalaible)
			) {
				continue;
			}

			$methodName = sprintf(
				self::CREATE_METHOD_SET_FORMAT,
				ucfirst($property)
			);

			$user->$methodName($value);
		}

		$resultUser = $user->update();

		$messages = [];

		if(!empty($user->getMessages())) {
			foreach ($user->getMessages() as $message) {
				$messages[] = $message->getMessage();
			}
		}

		$response =  array(
			self::RESPONSE_STATUS	=> $resultUser,
			self::RESPONSE_MESSAGES	=> $messages
		);

		return $response;
	}

	private function actionDelete() {}

	public static function createInstanceByType(
		Type $userType,
		int $uid
	) {

		$user = \api\controller\FactoryController::onCreate(
			$userType->getControllerClass()
		);

		return $user->getEntityById($uid);
	}
	/**
	 * @throws \api\exception\ApiException
	 */
	protected function ensureRequest() {
		if(empty($this->args)) {
			throw new \api\exception\ApiException(
				'Args can\'t be null'
			);
		};
	}

	protected static $propertyUpdateNotAvalaible = array(
		\api\entitys\user\User::PROPERTY_ID,
		\api\entitys\user\User::PROPERTY_USERNAME,
		\api\entitys\user\User::PROPERTY_STATUS
	);
}