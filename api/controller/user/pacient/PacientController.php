<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\controller\user\pacient;

class PacientController
	extends \api\controller\user\UserController {

    const CREATE_METHOD_SET_FORMAT  = 'set%s';
    const MODEL                     = \api\entitys\user\pacient\Pacient::class;

	const REDIS_KEY_ACTIVATE = 'fd:u.%s:hash.%s';

	const ARG_HASH = 'hash';

    const USER_TYPE = 1;

    public function onConstruct() {
		parent::onConstruct();
    }
	
	protected function ensureArgs() {
		foreach(self::$mandatoryPropertys as $property) {

			if(!array_key_exists($property , $this->args)) {
				throw new \Exception('need param ' . $property);
			}

		}
	}

	protected function setHashActivate($uid) {
		$driver = \cache\Driver::createFromConfig('redis');

		$cryptoStrong = true;

		$bytes = openssl_random_pseudo_bytes(16, $cryptoStrong);
		$hash = bin2hex($bytes);

		$key = $this->buildKeySession($uid,$hash);

		$driver->set(
			$key,
			['uid' => $uid]
		);

		$threeDaysInSecond = 259200;

		$driver->expire($key, $threeDaysInSecond);

		return $hash;
	}

	protected function actionActivate($uid){
    	/** @var \api\entitys\user\pacient\Pacient $user */
		$user = $this->model->getById($uid);

		if(
			!array_key_exists(self::ARG_HASH, $this->request->getQuery())
		) {
			throw new \api\exception\ApiException('need hash for activate');
		}

		$hash = $this->request->getQuery();

		$keyActivate = $this->buildKeySession($uid, $hash[self::ARG_HASH]);

		$driver = \cache\Driver::createFromConfig('redis');

		if($driver->keyExist($keyActivate)) {
			$user->setStatus(1);
			$result = $user->save();

			$messages = [];

			if(!empty($user->getMessages())) {
				foreach ($user->getMessages() as $message) {
					$messages[] = $message->getMessage();
				}
			}

			if($result) {
				$driver->delete($keyActivate);
			}

			$response =  array(
				self::RESPONSE_STATUS	=> $result,
				self::RESPONSE_MESSAGES	=> $messages
			);

			return $response;
		}

		throw new \api\exception\ApiException('hash not exist or expired, register again');


	}

	/**
	 * @param $uid
	 * @return string
	 */
	private function buildKeySession($uid,$hash) {
		return sprintf(
			self::REDIS_KEY_ACTIVATE,
			$uid,
			$hash
		);
	}

	private static $mandatoryPropertys = [
		\api\entitys\user\pacient\Pacient::PROPERTY_USERNAME,
		\api\entitys\user\pacient\Pacient::PROPERTY_INSURANCE_ID
	];

    /** @var \Phalcon\Mvc\Model $model */
    protected $model;
}