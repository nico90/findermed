<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\controller\user\professional;

class ProfessionalController
	extends \api\controller\user\UserController {

    const CREATE_METHOD_SET_FORMAT  = 'set%s';
    const MODEL                     = \api\entitys\user\professional\Professional::class;

	const USER_TYPE = 0;

	const ARG_CLINIC_ID			= "clinic_id";
	const ARG_DAYS				= "days";
	const ARG_DAY				= "day";
	const ARG_TIME_START_WORK	= "start";
	const ARG_TIME_END_WORK		= "end";
	const ARG_BREAKS			= "breaks";
	const ARG_TIME_BREAK_START	= "start";
	const ARG_TIME_BREAK_END	= "end";
	const ARG_START_VACATION	= "start";
	const ARG_END_VACATION		= "end";
	const ARG_CLINIC			= "clinic";

	const ARG_PROFESSIONAL_ID	= "professionalId";

	public function onConstruct() {
 		parent::onConstruct();
	}

	protected function ensureArgs() {

		foreach(self::$mandatoryPropertys as $property) {

			if(!array_key_exists($property , $this->args)) {
				throw new \Exception('need param ' . $property);
			}
		}
	}

	private function ensureFieldsTimeZone() {

		foreach(self::$mandatoryArgConfigTimeZone as $property) {

			if(!array_key_exists($property , $this->args)) {
				throw new \Exception('need param ' . $property);
			}
		}

		//it's necessary that days be greater than 0
		if(!count($this->args[self::ARG_DAYS]) > 0 ) {
			throw new \api\exception\EntityException(
				'You need to establish at least one day.'
			);
		}
	}

	private function ensureTimeZone(
		\api\entitys\user\professional\ProfessionalHourZones $professional
	) {

		$results =
			\api\entitys\user\professional\ProfessionalHourZones::getHourZonesByPidAndDay(
				$professional
			);

		$hourStart =
			\common\Common::toCamelCase(
				\api\entitys\user\professional\ProfessionalHourZones::PROPERTY_HOUR_START
			);

		$hourEnd =
			\common\Common::toCamelCase(
				\api\entitys\user\professional\ProfessionalHourZones::PROPERTY_HOUR_END
			);

		foreach ($results as $result) {
			if(
				!($professional->getHourStart() > $result[$hourEnd]) &&  //quiere empezar antes que termine el otro horario
				!($professional->getHourEnd() < $result[$hourStart]) //quiere terminar despues que termine el otro horario
			) {
				return false;
			}

		}

		return true;

	}

	/**
	 * REQUEST

	"args":{
		"clinic_id":1,
		"days":[
			{
				"day":1,
				"start":"09:00",
				"end":"18:00",
				"breaks":[
					{
						"start": "14:30",
						"end": "15:00"
					}
				]
			},
			{
				"day":2,
				"start":"09:00",
				"end":"18:00",
				"breaks":[
					{
						"start": "14:30",
						"end": "15:00"
					}
				]
			},
			{
				"day":3,
				"start":"09:00",
				"end":"18:00",
				"breaks":[
					{
						"start": "14:30",
						"end": "15:00"
					}
				]
			}
		],
		"time_appointment":10,
		"type_query_id":1
	}
	 * RESPONSE

	"response":{
		"status":true,
	 	"messages":[]
	}
	 */

	protected function actionConfigTimeZone() {

		$this->ensureFieldsTimeZone();

		/** @var \api\entitys\user\professional\Professional $professional */
		$professional =
			\api\entitys\user\professional\Professional::getEntityByUsername(
				$this->getUsernameByToken()
			);

		$resultConfig	= [];
		$messages 		= [];

//		\api\entitys\clinic\ClinicProfessional::

//		$this->checkExistRelation(
//			$professional->getId(),
//			$this->args[self::ARG_CLINIC_ID]
//		);

		foreach($this->args[self::ARG_DAYS] as $workSchedule) {
			/** @var \api\entitys\user\professional\ProfessionalHourZones $professionalhourZone */
			$professionalhourZone =
				\api\entitys\FactoryModel::onCreate(
					\api\entitys\user\professional\ProfessionalHourZones::class
				);

			$professionalhourZone->setClinicId($this->args[self::ARG_CLINIC_ID]);
			$professionalhourZone->setPid($professional->getId());
			$professionalhourZone->setDay($workSchedule[self::ARG_DAY]);
			$professionalhourZone->setHourStart($workSchedule[self::ARG_TIME_START_WORK]);
			$professionalhourZone->setHourEnd($workSchedule[self::ARG_TIME_END_WORK]);

			$hoursBreak =
				$workSchedule[self::ARG_BREAKS][self::ARG_TIME_BREAK_START] .
				'-' .
				$workSchedule[self::ARG_BREAKS][self::ARG_TIME_BREAK_END];

			$professionalhourZone->setBreakTime($hoursBreak);

			if(!$this->ensureTimeZone($professionalhourZone)) {

				$text = "Imposible config time zone becouse the day " .
					\api\controller\user\Days::getNameOf(
						$professionalhourZone->getDay()
					) .
					" is ocupated.";

				$field = "Time Zone day: " .
					\api\controller\user\Days::getNameOf(
						$professionalhourZone->getDay()
					);

				$type = "InvalidValue";
				$message = new \Phalcon\Mvc\Model\Message($text, $field, $type);
				$professionalhourZone->appendMessage($message);

				foreach ($professionalhourZone->getMessages() as $message) {
					$messages[] = $message->getMessage();
				}

				continue;
			}

			$resultConfig[
				\api\controller\user\Days::getNameOf($workSchedule[self::ARG_DAY])
			] =	$professionalhourZone->save();

		}


		$response = array(
			static::RESPONSE_STATUS		=> $resultConfig,
			static::RESPONSE_MESSAGES	=> $messages
		);

		return $response;
	}


	protected function actionConfigSetClinic() {

		/** @var \api\entitys\user\professional\Professional $professional */
		$professional =
			\api\entitys\user\professional\Professional::getEntityByUsername(
				$this->getUsernameByToken()
			);

		$messages 		= [];

		/**
		 * @var \api\entitys\clinic\Clinic $clinic
		 */
		$clinic =
			\api\entitys\FactoryModel::onCreate(
				\api\entitys\clinic\Clinic::class,
				$this->args[self::ARG_CLINIC]
			);

		$resultSetClinic = $clinic->save();

		$clinicProfessional = \api\entitys\FactoryModel::onCreate(
			\api\entitys\clinic\ClinicProfessional::class,
			[
				"professional_id"	=> $professional->getId(),
				"clinic_id"			=> $clinic->getId()
			]
		);

		$resultSetRelation =
			$clinicProfessional->save();

		if(!empty($clinicProfessional->getMessages())) {
			foreach ($clinicProfessional->getMessages() as $message) {
				$messages[] = $message->getMessage();
			}
		}

		if(!empty($clinic->getMessages())) {
			foreach ($clinic->getMessages() as $message) {
				$messages[] = $message->getMessage();
			}
		}

		$response =  array(
			static::RESPONSE_STATUS		=> $resultSetClinic,
			static::RESPONSE_MESSAGES	=> $messages
		);

		return $response;
	}

	protected function actionConfigSetVacation() {

		/** @var \api\entitys\user\professional\Professional $professional */
		$professional =
			\api\entitys\user\professional\Professional::getEntityByUsername(
				$this->getUsernameByToken()
			);

		$resultConfig	= [];
		$messages 		= [];

		$professionalVacationZone =
			\api\entitys\user\professional\ProfessionalVacationZone::getEntityByPid(
				$professional->getId()
			);

		if(empty($professionalVacationZone)) {
			/** @var \api\entitys\user\professional\ProfessionalVacationZone $professionalVacationZone */
			$professionalVacationZone =
				\api\entitys\FactoryModel::onCreate(
					\api\entitys\user\professional\ProfessionalVacationZone::class
				);
		}

		$professionalVacationZone->setPid(
			$professional->getId()
		);

		$professionalVacationZone->setStart(
			$this->args[self::ARG_START_VACATION]
		);

		$professionalVacationZone->setEnd(
			$this->args[self::ARG_END_VACATION]
		);

		$resultConfig = $professionalVacationZone->save();

		if(!empty($professionalVacationZone->getMessages())) {
			foreach ($professionalVacationZone->getMessages() as $message) {
				$messages[] = $message->getMessage();
			}
		}

		$response =  array(
			static::RESPONSE_STATUS		=> $resultConfig,
			static::RESPONSE_MESSAGES	=> $messages
		);

		return $response;
	}

	/** @var \Phalcon\Mvc\Model $model */
	protected $model;

	private static $mandatoryPropertys = [
		\api\entitys\user\professional\Professional::PROPERTY_USERNAME,
		\api\entitys\user\professional\Professional::PROPERTY_PROFESSIONAL_REGISTRATION,
		\api\entitys\user\professional\Professional::PROPERTY_SPECIALTY_ID,
	];

	/**
	 * Add here when need request token for action
	 * */
	protected static $actionNeedToken	= [
		0 => 'configTimeZone',
		1 => 'configSetVacation',
		2 => 'configSetClinic'
	];

	private static $mandatoryArgConfigTimeZone = [
		self::ARG_CLINIC_ID,
		self::ARG_DAYS
	];

}