<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 20/02/17
 * Time: 12:27
 */

namespace api\entitys;


abstract class Entity
    extends \Phalcon\Mvc\Model {

	use \common\ValueObjectTrait;

	const DB_NAME_APPT = 'dbAppointment';

	const ENABLE	= 1;
	const DISABLE	= 0;

    abstract protected function initialize();
    abstract protected function validation();

	abstract protected static function getEntityById(int $id);
}