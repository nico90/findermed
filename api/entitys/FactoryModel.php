<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 17/02/17
 * Time: 10:32
 */

namespace api\entitys;

class FactoryModel
    extends \api\Factory {

    /**
     * @param string $model
     * @return \Phalcon\Mvc\Model Controller
     * @throws \api\exception\ApiException
     * */
    static public function onCreate(
    	string $model,
		array $args = null
	): \Phalcon\Mvc\Model {
        if(class_exists($model)) {
            return new $model($args);
        }else{
            throw new \api\exception\ApiException('Model not exist');
        }
    }

}

