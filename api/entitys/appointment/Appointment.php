<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 29/10/16
 * Time: 14:15
 * */

namespace api\entitys\appointment;

use api\entitys\user\pacient\Pacient as Pacient;
use api\entitys\user\professional\Professional as Professional;

class Appointment
    extends \api\entitys\Entity {

	const PROPERTY_ID					= 'id';
	const PROPERTY_PACIENT_ID			= 'uid';
	const PROPERTY_PROFESSIONAL_ID		= 'professional_id';
	const PROPERTY_CLINIC_ID			= 'clinic_id';
	const PROPERTY_APPOINTMENT_START	= 'date_appointment_start';
	const PROPERTY_APPOINTMENT_END		= 'date_appointment_end';
	const PROPERTY_DATE_CREATED			= 'date_created';
	const PROPERTY_TYPE_QUERY_ID		= 'type_query_id';
	const PROPERTY_STATUS				= 'status';

	public function initialize() {
		$this->setConnectionService(\api\entitys\Entity::DB_NAME_APPT);
		$this->setSource("appointments");
	}

	public function columnMap() {
		return [
			self::PROPERTY_ID				=> 'id',
			self::PROPERTY_PACIENT_ID		=> 'uid',
			self::PROPERTY_PROFESSIONAL_ID	=> 'professionalId',
			self::PROPERTY_CLINIC_ID		=> 'clinicId',
			self::PROPERTY_APPOINTMENT_START=> 'dateAppointmentStart',
			self::PROPERTY_APPOINTMENT_END	=> 'dateAppointmentEnd',
			self::PROPERTY_DATE_CREATED		=> 'dateCreated',
			self::PROPERTY_TYPE_QUERY_ID	=> 'typeQueryId',
			self::PROPERTY_STATUS			=> 'status',
		];
	}

	public function validation() {}

	/**
	 * @param int $id
	 *
	 * @return \Phalcon\Mvc\Model\ResultsetInterface
	 */
	public static function getByIdAndPid(int $id, int $pid){

		$results = self::findFirst(
			[
				'conditions'	=>
					'id = ?1 AND professionalId = ?2',
				'bind'			=> [
					1 => $id,
					2 => $pid
				]
			]
		);

		return $results;
	}

	/**
	 * default return appointment pending
	 * @param int $pid
	 *
	 * @return array
	 */
	public static function getAppointmentByPid(
		int $pid,
		int $statusAp = 1
	) : array{

		$results = self::find(
			[
				'conditions'	=>
					'professionalId = ?1 AND '.
					'status = ?2' ,
				'bind'			=> [
					1 => $pid,
					2 => $statusAp,
				]
			]
		)->filter(function ($child) {
				return $child;
			});

		return $results;
	}

	/**
	 * default return appointment pending
	 *
	 * @param int $pid
	 *
	 * @param \DateTime $datetime
	 * @param array $statusAp
	 * @return array
	 *
	 * todo: ojo, esta sin terminar.
	 */
	public static function getAppointmentByPidAndDate(
		int $pid,
		\DateTime $datetime,
		int $statusAp
	) : array{


		$results = self::find(
			[
				'conditions'	=>
					'professionalId = ?1 AND '.
					'status = ?2 AND '.
					"dateAppointmentStart LIKE ?3",
				'bind'			=> [
					1 => $pid,
					2 => $statusAp,
					3 => $datetime->format('Y-m-d'). '%'
				]
			]
		)->filter(function ($child) {
			return $child;
		});


		return $results;
	}

	/**
	 * default return appointment pending
	 *
	 * @param int $pid
	 *
	 * @param \DateTime $datetime
	 * @param array $statusAp
	 * @return array
	 */
	public static function checkIfAppointmentIsAvalaible(
		int $pid,
		\DateTime $datetime,
		int $statusAp
	) {


		$results = self::find(
			[
				'conditions'	=>
					'professionalId = ?1 AND '.
					'status = ?2 AND '.
					"dateAppointmentStart LIKE ?3",
				'bind'			=> [
					1 => $pid,
					2 => $statusAp,
					3 => $datetime->format('Y-m-d'). '%'
				]
			]
		);

		return $results;
	}

	protected static function getEntityById(int $id) {
		// TODO: Implement getEntityById() method.
	}


	/**
	 * @param string $username
	 * @param bool $validateUserType
	 *
	 * @return \Phalcon\Mvc\Model
	 *
	 * @throws \Phalcon\Exception
	 * @internal param User $user
	 */
	public static function getByUsername(
		string $username,
		bool $validateUserType = true
	) {	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id) {
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getUid(): int {
		return $this->uid;
	}

	/**
	 * @param int $uid
	 */
	public function setUid(int $uid) {
		$this->uid = $uid;
	}

	/**
	 * @return int
	 */
	public function getProfessionalId(): int {
		return $this->professionalId;
	}

	/**
	 * @param int $professionalId
	 */
	public function setProfessionalId(int $professionalId) {
		$this->professionalId = $professionalId;
	}

	/**
	 * @return int
	 */
	public function getClinicId(): int {
		return $this->clinicId;
	}

	/**
	 * @param int $clinicId
	 */
	public function setClinicId(int $clinicId) {
		$this->clinicId = $clinicId;
	}


	/**
	 * @return \DateTime
	 */
	public function getDateAppointmentStart(): \DateTime {

		if(!empty($this->dateAppointmentStart)) {
			return new \DateTime($this->dateAppointmentStart);
		}
	}

	/**
	 * @param string $dateAppointmentStart
	 * @internal param string $dateAppointment
	 */
	public function setDateAppointmentStart(string $dateAppointmentStart) {
		$this->dateAppointmentStart = $dateAppointmentStart;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateAppointmentEnd(): \DateTime {
		return new \DateTime($this->dateAppointmentEnd);
	}

	/**
	 * @param string $dateAppointmentEnd
	 * @internal param string $dateAppointment
	 */
	public function setDateAppointmentEnd(string $dateAppointmentEnd) {
		$this->dateAppointmentEnd = $dateAppointmentEnd;
	}
	/**
	 * @return int
	 */
	public function getTypeQueryId(): int {
		return $this->typeQueryId;
	}

	/**
	 * @param int $setTypeQueryId
	 */
	public function setTypeQueryId(int $typeQueryId) {
		$this->typeQueryId = $typeQueryId;
	}

	/**
	 * @return \string
	 */
	public function getDateCreated(): \DateTime {
		return $this->dateCreated;
	}

	/**
	 * @param \DateTime $dateCreated
	 */
	public function setDateCreated(\DateTime $dateCreated) {
		$this->dateCreated = $dateCreated;
	}

	/**
	 * @return \api\controller\appointment\AppointmentStatusType
	 */
	public function getStatus(): \api\controller\appointment\AppointmentStatusType {
		return \api\controller\appointment\AppointmentStatusType::getInstanceById(
			(int) $this->status
		);
	}

	/**
	 * @param int $status
	 */
	public function setStatus($status) {
		$this->status = $status;

	}

	/**
	 * @param string $method
	 * @param mixed $arguments
	 *
	 * @throws \api\exception\EntityException
	 *
	 * @return void
	 */
	public function __call($method, $arguments) {
		throw new \api\exception\EntityException(
			$method . ' method is not exist in ' . static::class
		);
	}

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var int
	 */
	protected $uid;

	/**
	 * @var int
	 */
	protected $professionalId;

	/**
	 * @var int
	 */
	protected $clinicId;

	/**
	 * @var \DateTime
	 */
	protected $dateAppointmentStart;

	/**
	 * @var \DateTime
	 */
	protected $dateAppointmentEnd;

	/**
	 * @var int
	 */
	protected $typeQueryId;

	/**
	 * @var \DateTime
	 */
	protected $dateCreated;

	/**
	 * @var int
	 */
	protected $status;


}

