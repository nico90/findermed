<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 17/11/17
 * Time: 08:15
 * */

namespace api\entitys\clinic;

class Clinic
    extends \api\entitys\Entity {



	const PROPERTY_ID				= 'id';
	const PROPERTY_BUSINESS_NAME	= 'business_name';
	const PROPERTY_STREET			= 'street';
	const PROPERTY_CITY				= 'city';
	const PROPERTY_ZIP_CODE			= 'zip_code';
	const PROPERTY_COUNTRY			= 'country';
	const PROPERTY_TAX_NUMBER		= 'tax_number';
	const PROPERTY_PHONE			= 'phone';
	const PROPERTY_STATUS			= 'status';

	public function initialize() {
		$this->setSource("clinics");

		$model = ClinicProfessional::class;

		$this->hasMany(
			"id",
			$model,
			"professional_id",
			[
				'alias' => 'clinicProfessional',
				'foreignKey' => [
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE,
                ]
			]
		);

	}

	public function columnMap() {
		return [
			self::PROPERTY_ID				=> 'id',
			self::PROPERTY_BUSINESS_NAME	=> 'businessName',
			self::PROPERTY_STREET			=> 'street',
			self::PROPERTY_CITY				=> 'city',
			self::PROPERTY_ZIP_CODE			=> 'zipCode',
			self::PROPERTY_COUNTRY			=> 'country',
			self::PROPERTY_TAX_NUMBER		=> 'taxNumber',
			self::PROPERTY_PHONE 			=> 'phone',
			self::PROPERTY_STATUS 			=> 'status'
		];
	}

	public function validation() {

		$validation = new \Phalcon\Validation();

		$validation->add(
			self::PROPERTY_TAX_NUMBER,
			new \Phalcon\Validation\Validator\Uniqueness(
				[
					"message" => ":field must be unique"
				]
			)
		);

		return $this->validate($validation);
	}

	protected static function getEntityById(int $id) {

		$clinic = static::findFirst(
			[
				'conditions' => 'id = ?1 AND status = ?2',
				'bind'       => [
					1 => $id,
					2 => self::ENABLE
				]
			]
		);

		return $clinic;

	}

//	protected function beforeSave(){
//
//	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id = null) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getBusinessName(): string {
		return $this->businessName;
	}

	/**
	 * @param string $businessName
	 */
	public function setBusinessName(string $businessName) {
		$this->businessName = $businessName;
	}

	/**
	 * @return string
	 */
	public function getStreet(): string {
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet(string $street) {
		$this->street = $street;
	}

	/**
	 * @return string
	 */
	public function getCity(): string {
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity(string $city) {
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getCountry(): string {
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry(string $country) {
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getZipCode(): string {
		return $this->zipCode;
	}

	/**
	 * @param string $zipCode
	 */
	public function setZipCode(string $zipCode) {
		$this->zipCode = $zipCode;
	}

	/**
	 * @return string
	 */
	public function getTaxNumber(): string {
		return $this->taxNumber;
	}

	/**
	 * @param string $taxNumber
	 */
	public function setTaxNumber(string $taxNumber) {
		$this->taxNumber = $taxNumber;
	}

	/**
	 * @return string
	 */
	public function getPhone(): string {
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone(string $phone) {
		$this->phone = $phone;
	}

	/**
	 * @return int
	 */
	public function getStatus(): int {
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus(int $status = null) {
		$this->status = $status;
	}

	/**
	 * @param string $method
	 * @param mixed $arguments
	 *
	 * @throws \api\exception\EntityException
	 *
	 * @return void
	 */
	public function __call($method, $arguments) {
		throw new \api\exception\EntityException($method . ' method is not exist in Clinic');
	}

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $businessName;

	/**
	 * @var string
	 */
	protected $street;

	/**
	 * @var string
	 */
	protected $city;

	/**
	 * @var string
	 */
	protected $country;

	/**
	 * @var string
	 */
	protected $zipCode;

	/**
	 * @var string
	 */
	protected $taxNumber;

	/**
	 * @var string
	 */
	protected $phone;

	/**
	 * @var int
	 */
	protected $status;

}

