<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 17/11/17
 * Time: 08:15
 * */

namespace api\entitys\clinic;

class ClinicProfessional
    extends \api\entitys\Entity {

	const PROPERTY_CLINIC_ID				= 'clinic_id';
	const PROPERTY_PROFESSIONAL_ID	= 'professional_id';

	public function initialize() {
		$this->setSource("clinic_professional");
	}

	public function validation() {}

	protected static function getEntityById(int $id) {

		$clinic = static::findFirst(
			[
				'conditions' => 'id = ?1',
				'bind'       => [
					1 => $id
				]
			]
		);

		return $clinic;

	}

	public function columnMap() {
		return [
			self::PROPERTY_CLINIC_ID		=> 'clinicId',
			self::PROPERTY_PROFESSIONAL_ID	=> 'professionalId'
		];
	}

	/**
	 * @return int
	 */
	public function getClinicId(): int {
		return $this->clinicId;
	}

	/**
	 * @param int $clinicId
	 */
	public function setClinicId(int $clinicId) {
		$this->clinicId = $clinicId;
	}

	/**
	 * @return int
	 */
	public function getProfessionalId(): int {
		return $this->professionalId;
	}

	/**
	 * @param int $professionalId
	 */
	public function setProfessionalId(int $professionalId) {
		$this->professionalId = $professionalId;
	}
//
//	protected static function getEntityByPid(int $id) {
//
//		$clinic = static::findFirst(
//			[
//				'conditions' => 'id = ?1',
//				'bind'       => [
//					1 => $id
//				]
//			]
//		);
//
//		return $clinic;
//
//	}



	/**
	 * @param string $method
	 * @param mixed $arguments
	 *
	 * @throws \api\exception\EntityException
	 *
	 * @return void
	 */
	public function __call($method, $arguments) {
		throw new \api\exception\EntityException($method . ' method is not exist in ClinicProfessional');
	}

	/**
	 * @var int
	 */
	protected $clinicId;

	/**
	 * @var int
	 */
	protected $professionalId;

}

