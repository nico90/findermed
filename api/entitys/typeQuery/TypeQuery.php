<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 29/10/16
 * Time: 14:15
 * */

namespace api\entitys\typeQuery;

use api\entitys\user\pacient\Pacient as Pacient;
use api\entitys\user\professional\Professional as Professional;

class TypeQuery
    extends \api\entitys\Entity {

	const PROPERTY_ID				= 'id';
	const PROPERTY_DESCRIPTION		= 'description';
	const PROPERTY_DURATION			= 'duration';
	const PROPERTY_UID				= 'uid';


	public function initialize() {
		$this->setConnectionService(\api\entitys\Entity::DB_NAME_APPT);
		$this->setSource("type_query");

	}

	public function validation() {}

	/**
	 * @param int $id
	 *
	 * @return \Phalcon\Mvc\Model
	 */
	public static function getById(int $id){

		$typeQuery = static::findFirst(
			[
				'conditions' => 'id = ?1',
				'bind'       => [
					1 => $id
				]
			]
		);

		return $typeQuery;

	}


	/**
	 * @param int $id
	 *
	 * @return \Phalcon\Mvc\Model
	 */
	public static function getEntityById(int $id) : array{

	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description) {
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getDuration(): string {
		return $this->duration;
	}

	/**
	 * @param string $duration
	 */
	public function setDuration(string $duration) {
		$this->duration = $duration;
	}

	/**
	 * @return int
	 */
	public function getUid(): int {
		return $this->uid;
	}

	/**
	 * @param int $uid
	 */
	public function setUid(int $uid) {
		$this->uid = $uid;
	}

	/**
	 * @param string $method
	 * @param mixed $arguments
	 *
	 * @throws \api\exception\EntityException
	 *
	 * @return void
	 */
	public function __call($method, $arguments) {
		throw new \api\exception\EntityException(
			$method . ' method is not exist in ' . static::class
		);
	}

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $description;

	/**
	 * @var string
	 */
	protected $duration;


	/**
	 * @var int
	 */
	protected $uid;


}

