<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 29/10/16
 * Time: 14:15
 * */

namespace api\entitys\user;

use api\entitys\user\pacient\Pacient as Pacient;
use api\entitys\user\professional\Professional as Professional;

class User
    extends \api\entitys\Entity {

	const PROPERTY_ID			= 'id';
	const PROPERTY_USERNAME		= 'username';
	const PROPERTY_USER_TYPE	= 'usertype';
	const PROPERTY_PASSWORD		= 'password';
	const PROPERTY_LAST_NAME	= 'lastname';
	const PROPERTY_NAME			= 'name';
	const PROPERTY_DU			= 'du';
	const PROPERTY_GENDER		= 'gender';
	const PROPERTY_PHONE		= 'phone';
	const PROPERTY_EMAIL		= 'email';
	const PROPERTY_BIRTHDATE	= 'birthdate';
	const PROPERTY_STATUS		= 'status';

	const USER_TYPE = null;

	public function onConstruct(array $properties = null, $setAlways = false) {}

	public function initialize() {
		$this->setSource("users");

		$modelAppointment = \api\entitys\appointment\Appointment::class;

		$type = static::USER_TYPE;

		$referenceField = null;

		if(isset($type)) {
			switch ($type) {
				case 0:
					$referenceField = 'professionalId';//\api\entitys\appointment\Appointment::PROPERTY_PROFESSIONAL_ID;
					break;
				case 1:
					$referenceField = \api\entitys\appointment\Appointment::PROPERTY_PACIENT_ID;
					break;
				default:
					$referenceField = null;
			}
		}

		$this->hasMany(
			"id",
			$modelAppointment,
			$referenceField, //este campo se tiene que definir dependiendo si el token que registramos es de un profesional o un paciente.
			['alias' => 'appointments']
		);
	}

	public function validation() {

		$validation = new \Phalcon\Validation();

		$validation->add(
			self::PROPERTY_USERNAME,
			new \Phalcon\Validation\Validator\Uniqueness(
				[
					"message" => ":field must be unique"
				]
			)
		);

		$validation->add(
			self::PROPERTY_DU,
		
			new \Phalcon\Validation\Validator\Uniqueness(
				[
					"message" => ":field must be unique"
				]
			)
		);

		$validation->add(
			self::PROPERTY_PHONE,
			new \Phalcon\Validation\Validator\Uniqueness(
				[
					"message" => ":field must be unique"
				]
			)
		);

		$validation->add(
			self::PROPERTY_EMAIL,
			new \Phalcon\Validation\Validator\Uniqueness(
				[
					"message" => ":field must be unique"
				]
			)
		);

		return $validation;
	}

	/**
	 * @param int $id
	 * @return \Phalcon\Mvc\Model
	 * @throws \Phalcon\Exception
	 */
	public static function getById(int $id){

		$user = static::findFirst(
			[
				'conditions' => 'id = ?1 AND usertype =?2',
				'bind'       => [
					1 => $id,
					2 => static::USER_TYPE
				],
				'columns' => [
					self::PROPERTY_ID,
					self::PROPERTY_USERNAME,
					self::PROPERTY_LAST_NAME,
					self::PROPERTY_NAME,
					self::PROPERTY_DU,
					self::PROPERTY_BIRTHDATE,
					self::PROPERTY_USER_TYPE,
					Professional::PROPERTY_SPECIALTY_ID,
					Professional::PROPERTY_PROFESSIONAL_REGISTRATION,
					Pacient::PROPERTY_INSURANCE_ID,
					self::PROPERTY_STATUS
				]
			]
		);

		if(empty($user)) {
			throw new \Phalcon\Exception('id doesnt exist for any ' . static::class);
		}

		$user = self::prepareResponseForGet($user);

		return array_filter((array) $user);

	}

	/**
	 * @param int $id
	 * @return \Phalcon\Mvc\Model
	 *
	 * @throws \Phalcon\Exception
	 */
	public static function getEntityById(int $id) : self{

		$user = static::findFirst(
			[
				'conditions' => 'id = ?1 AND usertype =?2',
				'bind'       => [
					1 => $id,
					2 => static::USER_TYPE
				]
			]
		);

		if(empty($user)) {
			throw new \Phalcon\Exception('id doesnt exist for any ' . static::class);
		}

		return $user;
	}

	/**
	 * @param string $username
	 * @param bool $validateUserType
	 *
	 * @return \Phalcon\Mvc\Model
	 *
	 * @throws \Phalcon\Exception
	 * @internal param User $user
	 */
	public static function getByUsername(
		string $username,
		bool $validateUserType = true
	) {

		if($validateUserType){
			$query = 'username = ?1 AND usertype =?2';
			$bind	= [
				1 => $username,
				2 => static::USER_TYPE
			];
		}else {
			$query = 'username = ?1';
			$bind  = [
				1 => $username
			];
		}

		$user = static::findFirst(
			[
				'conditions' => $query,
				'bind'       => $bind,
				'columns' => [
					self::PROPERTY_ID,
					self::PROPERTY_USERNAME,
					self::PROPERTY_LAST_NAME,
					self::PROPERTY_NAME,
					self::PROPERTY_DU,
					self::PROPERTY_BIRTHDATE,
					self::PROPERTY_USER_TYPE,
					Professional::PROPERTY_SPECIALTY_ID,
					Professional::PROPERTY_PROFESSIONAL_REGISTRATION,
					Pacient::PROPERTY_INSURANCE_ID,
					self::PROPERTY_STATUS
				]
			]
		);

		if(empty($user)) {
			throw new \Phalcon\Exception('username doesnt exist for any ' . static::class);
		}

		$user = self::prepareResponseForGet($user);

		return $user;

	}

	/**
	 * @param string $username
	 * @return \Phalcon\Mvc\Model
	 * @throws \Phalcon\Exception
	 * @internal param User $user
	 */
	public static function getEntityByUsername(
		string $username
	) : \Phalcon\Mvc\Model {

		$user = static::findFirst(
			[
				'conditions' => 'username = ?1 AND usertype = ?2',
				'bind'       => [
						1 => $username,
						2 => static::USER_TYPE
				]
			]
		);

		if(empty($user)) {
			throw new \Phalcon\Exception('username doesn\'t exist for any ' . static::class);
		}

		return $user;

	}

	private static function prepareResponseForGet($user) {

		$user->usertype = \api\controller\user\Type::getNameOf(
			(int) $user->usertype
		);

		if(!empty($user->specialtyId)) {

			$specialty =
				\api\entitys\user\professional\Specialty::getEntityById(
					$user->specialtyId
				);

			$user->specialty = $specialty->getDescription();
			$user->specialtyId = null;

		}

		if(!empty($user->insuranceId)) {

			$insuranceId =
				\api\entitys\user\pacient\Insurance::getEntityById(
					$user->insuranceId
				);


			$user->insurance = $insuranceId->getDescription();
			$user->insuranceId = null;

		}

		if(!empty($user->password)) {

			$user->password = null;

		}

		return $user;
	}

	/**
	 * @param \auth\Credential $credential
	 *
	 * @return \Phalcon\Mvc\Model
	 *
	 * @throws \api\exception\ApiException
	 */
	public function findForLogin (
		\auth\Credential $credential
	): \Phalcon\Mvc\Model {

		$user = static::findFirst(
			[
				'conditions' => 'username = ?1 AND password = ?2',
				'bind'       => [
					1 => $credential->getUsername(),
					2 => $credential->getPassword()
				]
			]
		);

		if(empty($user)) {

			$user = static::findFirst(
				[
					'conditions' => 'email = ?1 AND password = ?2',
					'bind'       => [
						1 => $credential->getUsername(),
						2 => $credential->getPassword()
					]
				]
			);

			if(empty($user)) {
				throw new \api\exception\ApiException('Credential incorrect');
			}
		}

		return $user;
	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getUsername(): string {
		return $this->username;
	}

	/**
	 * @param string $username
	 */
	public function setUsername(string $username) {
		$this->username = $username;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string {
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword(string $password) {
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getLastname(): string {
		return $this->lastname;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastname(string $lastname) {
		$this->lastname = $lastname;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return \api\controller\user\Type
	 */
	public function getUsertype(): \api\controller\user\Type {
		$type = (int) $this->usertype;

		return \api\controller\user\Type::getInstanceById($type);
	}

	/**
	 * @param int
	 */
	public function setUsertype(int $usertype) {
		$this->usertype = $usertype;
	}

	/**
	 * @return int
	 */
	public function getDu(): int {
		return $this->du;
	}

	/**
	 * @param int $du
	 */
	public function setDu(int $du) {
		$this->du = $du;
	}

	/**
	 * @return string
	 */
	public function getGender(): string {
		return $this->gender;
	}

	/**
	 * @param string $gender
	 */
	public function setGender(string $gender) {
		$this->gender = $gender;
	}


	/**
	 * @return string
	 */
	public function getPhone(): string {
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone(string $phone) {
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail(string $email) {
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getBirthdate() {
		return $this->birthdate;
	}

	/**
	 * @param \DateTime $birthdate
	 */
	public function setBirthdate($birthdate) {

		$date =  new \DateTime($birthdate);

		$this->birthdate = $date->format('Y-m-d');
	}

	/**
	 * @return int
	 */
	public function getStatus(): int {
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus(int $status) {
		$this->status = $status;
	}

	/**
	 * @return array
	 */
	public function getAppointments() {
		return $this->appointments;
	}

	/**
	 * @param \api\entitys\appointment\Appointment $appointments
	 */
	public function setAppointments(
		\api\entitys\appointment\Appointment $appointments
	) {
		$this->appointments = $appointments;
	}



	/**
	 * @param string $method
	 * @param mixed $arguments
	 *
	 * @throws \api\exception\EntityException
	 *
	 * @return void
	 */
	public function __call($method, $arguments) {
		throw new \api\exception\EntityException(
			$method . ' method is not exist in ' . static::class
		);
	}

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $username;

	/**
	 * @var \api\controller\user\Type
	 */
	protected $usertype;

	/**
	 * @var string
	 */
	protected $password;

	/**
	 * @var string
	 */
	protected $lastname;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var int
	 */
	protected $du;

	/**
	 * @var string
	 */
	protected $gender;

	/**
	 * @var string
	 */
	protected $phone;

	/**
	 * @var string
	 */
	protected $email;

	/**
	 * @var \DateTime
	 */
	protected $birthdate;

	/**
	 * @var int
	 */
	protected $status;

	/**
	 * @var \api\entitys\appointment\Appointment[]
	 */
	protected $appointments;

}

