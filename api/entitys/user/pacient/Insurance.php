<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\entitys\user\pacient;

class Insurance
	extends \api\entitys\Entity {

	const PROPERTY_ID				= 'id';
	const PROPERTY_DESCRIPTION		= 'description';

	public function initialize() {
		$this->setSource("medical_insurances");
	}

	final public function validation() {}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	public static function getEntityById(
		int $id
	) : self {

		return self::findFirst(
			[
				'conditions' => 'id = ?1',
				'bind'       => [
					1 => $id
				]
			]
		);
	}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	public static function getInsurancesByIds(
		array $ids
	) : array {

		$insurances = [];

		foreach($ids as $id) {

			$insurance = Insurance::findFirst(
				[
					'columns'		=> self::PROPERTY_DESCRIPTION,
					'conditions'	=> 'id = ?1',
					'bind'			=> [
						1 => $id[Pacient::PROPERTY_INSURANCE_ID]
					]
				]
			)->toArray();

			$insurances[Pacient::PROPERTY_INSURANCE_ID][] =
				$insurance[self::PROPERTY_DESCRIPTION];
		}

		return $insurances;
	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description) {
		$this->description = $description;
	}

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $description;

}