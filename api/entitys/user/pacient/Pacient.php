<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\entitys\user\pacient;

class Pacient
	extends \api\entitys\user\User {

	const PROPERTY_INSURANCE_ID		= 'insuranceId';

	const USER_TYPE = 1;

	public function initialize() {
		parent::initialize();
	}

	final public function validation() {

		$validation = parent::validation();

		$validation->add(
			self::PROPERTY_INSURANCE_ID,
			new \Phalcon\Validation\Validator\Numericality(
				[
					"message" => ":field must be int"
				]
			)
		);


		return $this->validate($validation);
	}

	/**
	 * @return int
	 */
	public function getInsuranceId(): int {
		return $this->insuranceId;
	}

	/**
	 * @param int $insuranceId
	 */
	public function setInsuranceId(int $insuranceId) {
		$this->insuranceId = $insuranceId;
	}

	/**
	 * @param int $uid
	 * @internal param int $id
	 *
	 * @return array
	 */
	public static function getExtraByUid(
		int $uid
	) : array {

		$insuranceIds = self::find(
			[
				'columns'		=> self::PROPERTY_INSURANCE_ID,
				'conditions'	=> 'uid = ?1',
				'bind'			=> [
					1 => $uid
				]
			]
		)->toArray();

		$insurances = \api\entitys\user\pacient\Insurance::getInsurancesByIds(
			$insuranceIds
		);

		return $insurances;
	}

	/**
	 * Send a confirmation e-mail to the user after create the account
	 */
	public function afterSave() {

//		$mail = $this->getDI()->getMail()->send(
//			array(
//				 'nicolasandreoli9@gmail.com' => 'nicoksq'
//			),
//			"Please confirm your email",
//			'confirmation',
//			array(
//				'confirmUrl' => '/confirm/',
//				'publicUrl' => '/hashtuvieja/'
//			)
//		);
//
//		var_dump($mail);
	}

	/**
	 * @var int
	 */
	protected $insuranceId;

}