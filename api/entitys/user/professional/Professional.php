<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\entitys\user\professional;

class Professional
	extends \api\entitys\user\User {

	const PROPERTY_SPECIALTY_ID					= 'specialtyId';
	const PROPERTY_PROFESSIONAL_REGISTRATION	= 'professionalRegistration';
	const PROPERTY_TIME_HOURS					= 'timeHours';

	const USER_TYPE = 0;

	public function initialize() {
		parent::initialize();

		$modelHourZones = \api\entitys\user\professional\ProfessionalHourZones::class;

		$this->hasMany(
			"id",
			$modelHourZones,
			"pid",
			['alias' => 'workTimeZone']
		);

		$modelVacationZone = \api\entitys\user\professional\ProfessionalVacationZone::class;

		$this->hasMany(
			"id",
			$modelVacationZone,
			"pid",
			['alias' => 'vacationTimeZone']
		);

	}

	/**
	 * @param int $id
	 * @return \Phalcon\Mvc\Model
	 * @throws \Phalcon\Exception
	 */
	public static function getById(int $id) {

		$user = static::findFirst(
			[
				'conditions' => 'id = ?1 AND usertype =?2',
				'bind' => [
					1 => $id,
					2 => static::USER_TYPE
				]
			]
		);

		return $user;
	}


	final public function validation() {

		$validation = parent::validation();

		$validation->add(
			self::PROPERTY_SPECIALTY_ID,
			new \Phalcon\Validation\Validator\Numericality(
				[
					"message" => ":field must be int"
				]
			)
		);

		$validation->add(
			self::PROPERTY_PROFESSIONAL_REGISTRATION,
			new \Phalcon\Validation\Validator\Numericality(
				[
					"message" => ":field must be int"
				]
			)
		);

		return $this->validate($validation);

	}

	/**
	 * @param int $uid
	 * @internal param int $id
	 *
	 * @return array
	 */
	public static function getExtraByUid(
		int $uid
	) : array {

		$specialties = self::find(
			[
				'columns'		=> [
					\common\Common::toCamelCase(self::PROPERTY_SPECIALTY_ID),
					\common\Common::toCamelCase(self::PROPERTY_PROFESSIONAL_REGISTRATION)
				],
				'conditions'	=> 'uid = ?1',
				'bind'			=> [
					1 => $uid
				]
			]
		)->toArray();

		foreach($specialties as $key => $specialty) {

			$specialty = \api\entitys\user\professional\Specialty::getSpecialtyById(
				$specialty[\common\Common::toCamelCase(self::PROPERTY_SPECIALTY_ID)]
			);

			$specialties[$key][\common\Common::toCamelCase(self::PROPERTY_SPECIALTY_ID)] =
				$specialty[Specialty::PROPERTY_DESCRIPTION];
		}

		return $specialties;
	}

	/**
	 * @param int $id
	 * @param null $datetime
	 *
	 * @return \Phalcon\Mvc\Model
	 * @throws \Phalcon\Exception
	 */
	public static function getByIdWithAppointment(int $id, $datetime = null){

		$user = static::findFirst(
			[
				'conditions' => 'id = ?1 AND usertype =?2',
				'bind'       => [
					1 => $id,
					2 => static::USER_TYPE
				]
			]
		);

		$user->appointments =
			\api\entitys\appointment\Appointment::getAppointmentByPidAndDate(
				$user->getId(),
				$datetime,
				\api\controller\appointment\AppointmentStatusType::getIdOf(
					\api\controller\appointment\AppointmentStatusType::IN_PROGRESS
				)
			);

		if(empty($user)) {
			throw new \Phalcon\Exception('id doesnt exist for any ' . static::class);
		}

		return $user;
	}

	/**
	 * @return int
	 */
	public function getSpecialtyId(): int {
		return $this->specialtyId;
	}

	/**
	 * @param int $specialtyId
	 */
	public function setSpecialtyId(int $specialtyId) {

		$this->specialtyId = $specialtyId;
	}

	/**
	 * @return string
	 */
	public function getProfessionalRegistration(): string {
		return $this->professionalRegistration;
	}

	/**
	 * @param string $professionalRegistration
	 */
	public function setProfessionalRegistration(
		string $professionalRegistration
	) {
		$this->professionalRegistration = $professionalRegistration;
	}

	/**
	 * @return \api\entitys\user\professional\ProfessionalHourZones
	 */
	public function getWorkTimeZone() {
		return $this->workTimeZone;
	}

	/**
	 * @param \api\entitys\user\professional\ProfessionalHourZones $workTimeZone
	 */
	public function setWorkTimeZone(
		$workTimeZone = null
	) {
		$this->workTimeZone = $workTimeZone;
	}

	/**
	 * @return ProfessionalVacationZone
	 */
	public function getVacationTimeZone(): ProfessionalVacationZone {
		return $this->vacationTimeZone;
	}

	/**
	 * @param ProfessionalVacationZone $vacationTimeZone
	 */
	public function setVacationTimeZone($vacationTimeZone) {
		$this->vacationTimeZone = $vacationTimeZone;
	}


	/**
	 * @var int
	 */
	protected $specialtyId;

	/**
	 * @var string
	 */
	protected $professionalRegistration;

	/**
	 * @var array
	 */
	protected $clinics;

	/**
	 * @var \api\entitys\appointment\Appointment
	 */
	protected $appointments;

	/**
	 * @var \api\entitys\user\professional\ProfessionalHourZones
	 */
	protected $workTimeZone;

	/**
	 * @var \api\entitys\user\professional\ProfessionalVacationZone
	 */
	protected $vacationTimeZone;

}