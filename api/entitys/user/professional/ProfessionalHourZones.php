<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\entitys\user\professional;

class ProfessionalHourZones
	extends \api\entitys\Entity {

	const PROPERTY_PROFESSIONAL_ID	= 'pid';
	const PROPERTY_CLINIC_ID		= 'clinic_id';
	const PROPERTY_DAY				= 'day';
	const PROPERTY_HOUR_START		= 'hour_start';
	const PROPERTY_HOUR_END			= 'hour_end';
	const PROPERTY_BREAK_TIME		= 'break_time';
	const PROPERTY_STATUS			= 'status';

	const USER_TYPE				= 0;


	public function initialize() {
		$this->setSource("professional_hour_zones");
	}

	public function columnMap() {
		return [
			self::PROPERTY_PROFESSIONAL_ID	=> 'pid',
			self::PROPERTY_CLINIC_ID		=> 'clinicId',
			self::PROPERTY_DAY				=> 'day',
			self::PROPERTY_HOUR_START		=> 'hourStart',
			self::PROPERTY_HOUR_END			=> 'hourEnd',
			self::PROPERTY_BREAK_TIME 		=> 'breakTime',
			self::PROPERTY_STATUS 			=> 'status'
		];
	}

	final public function validation() {}

	protected static function getEntityById(int $id) {

//		$results = self::find(
//			[
//				'conditions'	=>
//					\api\entitys\user\professional\ProfessionalHourZones::PROPERTY_PROFESSIONAL_ID .
//					'= ?1 AND ' .
//					\api\entitys\user\professional\ProfessionalHourZones::PROPERTY_STATUS.
//					'=?2',
//				'bind'			=> [
//					1 => $id,
//					2 => 1
//				]
//			]
//		)->toArray();
//
//		return $results;
	}

	public static function getHourZonesByPid(int $pid) : array{

		$results = self::find(
			[
				'conditions'	=>
					\api\entitys\user\professional\ProfessionalHourZones::PROPERTY_PROFESSIONAL_ID .
					'= ?1 AND ' .
					\api\entitys\user\professional\ProfessionalHourZones::PROPERTY_STATUS.
					'= ?2',
				'bind'			=> [
					1 => $pid,
					2 => 1
				]
			]
		)->toArray();

		return $results;
	}


	public static function getHourZonesByPidAndDay(
		\api\entitys\user\professional\ProfessionalHourZones $professional
	)  : array {

		$results = self::find(
			[
				'conditions'	=>
					\api\entitys\user\professional\ProfessionalHourZones::PROPERTY_PROFESSIONAL_ID .
					'= ?1 AND ' .
					\api\entitys\user\professional\ProfessionalHourZones::PROPERTY_DAY.
					'= ?3',
				'bind'			=> [
					1 => $professional->getPid(),
					3 => $professional->getDay()
				]
			]
		)->toArray();

		return $results;
	}



	/**
	 * @return int
	 */
	public function getPid(): int {
		return $this->pid;
	}

	/**
	 * @param int $pid
	 */
	public function setPid(int $pid) {
		$this->pid = $pid;
	}

	/**
	 * @return int
	 */
	public function getClinicId(): int {
		return $this->clinicId;
	}

	/**
	 * @param int $clinicId
	 */
	public function setClinicId(int $clinicId) {
		$this->clinicId = $clinicId;
	}

	/**
	 * @return int
	 */
	public function getDay(): int {
		return $this->day;
	}

	/**
	 * @param int $day
	 */
	public function setDay(int $day) {
		$this->day = $day;
	}

	/**
	 * @return string
	 */
	public function getHourStart(): string {
		return $this->hourStart;
	}

	/**
	 * @param string $hourStart
	 */
	public function setHourStart(string $hourStart) {
		$this->hourStart = $hourStart;
	}

	/**
	 * @return string
	 */
	public function getHourEnd(): string {
		return $this->hourEnd;
	}

	/**
	 * @param string $hourEnd
	 */
	public function setHourEnd(string $hourEnd) {
		$this->hourEnd = $hourEnd;
	}

	/**
	 * @return string
	 */
	public function getBreakTime(): string {
		return $this->breakTime;
	}

	/**
	 * @param string $breakTime
	 */
	public function setBreakTime(string $breakTime) {
		$this->breakTime = $breakTime;
	}

	/**
	 * @return int
	 */
	public function getStatus(): int {
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus(int $status) {
		$this->status = $status;
	}

	/**
	 * @var int
	 */
	private $pid;

	/**
	 * @var int
	 */
	private $clinicId;

	/**
	 * @var int
	 */
	private $day;

	/**
	 * @var string
	 */
	private $hourStart;

	/**
	 * @var string
	 */
	private $hourEnd;

	/**
	 * @var string
	 */
	private $breakTime;

	/**
	 * @var int
	 */
	private $status;

}