<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\entitys\user\professional;

class ProfessionalVacationZone
	extends \api\entitys\Entity {

	const PROPERTY_PROFESSIONAL_ID	= 'pid';
	const PROPERTY_START			= 'start';
	const PROPERTY_END				= 'end';

	const USER_TYPE				= 0;

	public function initialize() {
		$this->setSource("professional_vacation_zone");
	}

//	public function columnMap() {
//		return [
//			self::PROPERTY_PROFESSIONAL_ID	=> 'pid',
//			self::PROPERTY_START			=> 'start',
//			self::PROPERTY_END				=> 'end',
//		];
//	}

	final public function validation() {}

	protected static function getEntityById(int $id) {
		// TODO: Implement getEntityById() method.
	}

	public static function getEntityByPid(int $pid) {

		$results = self::findFirst(
			[
				'conditions'	=>
					\api\entitys\user\professional\ProfessionalVacationZone::PROPERTY_PROFESSIONAL_ID .
					'= ?1',
				'bind'			=> [
					1 => $pid,
				]
			]
		);

		return $results;
	}

	/**
	 * @return int
	 */
	public function getPid(): int {
		return $this->pid;
	}

	/**
	 * @param int $pid
	 */
	public function setPid(int $pid) {
		$this->pid = $pid;
	}

	/**
	 * @return string
	 */
	public function getStart(): string {
		return $this->start;
	}

	/**
	 * @param string $start
	 */
	public function setStart(string $start) {
		$this->start = $start;
	}

	/**
	 * @return string
	 */
	public function getEnd(): string {
		return $this->end;
	}

	/**
	 * @param string $end
	 */
	public function setEnd(string $end) {
		$this->end = $end;
	}

	/**
	 * @var int
	 */
	private $pid;

	/**
	 * @var string
	 */
	private $start;

	/**
	 * @var string
	 */
	private $end;


}