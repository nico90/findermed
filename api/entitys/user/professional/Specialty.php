<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 23/11/16
 * Time: 19:59
 */

namespace api\entitys\user\professional;

class Specialty
	extends \api\entitys\Entity {

	const PROPERTY_ID				= 'id';
	const PROPERTY_DESCRIPTION		= 'description';

	public function initialize() {
		$this->setSource("dict_specialty");
	}

	final public function validation() {}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	public static function getEntityById(
		int $id
	) : self {

		return self::findFirst(
			[
				'conditions' => 'id = ?1',
				'bind'       => [
					1 => $id
				]
			]
		);
	}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	public static function getSpecialtyById(
		int $ids
	) : array {

			$specialty = Specialty::findFirst(
				[
					'columns'		=> self::PROPERTY_DESCRIPTION,
					'conditions'	=> 'id = ?1',
					'bind'			=> [
						1 => $ids
					]
				]
			)->toArray();

		return $specialty;
	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description) {
		$this->description = $description;
	}

	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $description;

}