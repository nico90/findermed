<?php
/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 01/11/16
 * Time: 12:31
 */

namespace api\exception;


class ApiException extends \Exception {
    const CODE      = 501;

    public function __construct(
        $message,
        $code = self::CODE,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

}