<?php
/**
 * Created by PhpStorm.
 * User: nicoksq
 * Date: 29/10/16
 * Time: 15:27
 */

namespace api\exception;

//TODO: change error code for not found entity or not found action.

class EntityException extends \Exception {

    const ENTITY_NOT_FOUND = 1;

    public function __construct($message, $code = 0, \Exception $previous = null) {

        parent::__construct($message, $code, $previous);
    }
}