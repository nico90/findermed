<?php

/**
 * Created by IntelliJ IDEA.
 * User: nico
 * Date: 21/09/17
 * Time: 12:37
 */

namespace security;

class Security extends \Phalcon\Mvc\Model {

	private $ip;

	private $token;

	public function initialize() {
		$this->setSource("security_clinic");
	}

	/**
	 * @param $name
	 * @param $arguments
	 */
	public function __call($name, $arguments) {

	}

	public static function existToken(string $token = null) : bool{

		$security = static::findFirst(
			[
				'conditions' => 'token = ?1',
				'bind'       => [
					1 => $token
				]
			]
		);

		return !empty($security);

	}

	/**
	 * @return mixed
	 */
	public function getIp() {
		return $this->ip;
	}

	/**
	 * @param mixed $ip
	 */
	public function setIp($ip) {
		$this->ip = $ip;
	}

	/**
	 * @return mixed
	 */
	public function getToken() {
		return $this->token;
	}

	/**
	 * @param mixed $token
	 */
	public function setToken($token) {
		$this->token = $token;
	}


}