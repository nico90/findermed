<?php
/**
 * Created by PhpStorm.
 * user: nicoksq
 * Date: 27/10/16
 * Time: 23:38
 */


//	EL PROGRESO SE NOS DEBE A LOS INSATISFECHOS //

use api\entitys\Entity;
use common\Mail;
use Phalcon\Loader;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Mvc\View;

//Creates the autoloader
$loader = new Phalcon\Loader();

$config = new Phalcon\Config\Adapter\Ini("config/curr/config.ini");

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di = new Phalcon\DI\FactoryDefault();


$di->set('db', function () use ($config) {

	$adapter = new PdoMysql(
		(array) $config->DatabaseFinderMed
	);

	return $adapter;
});

$di->set('dbAppointment', function () use ($config) {

	$adapter = new PdoMysql(
		(array) $config->DatabaseAppointment
	);

	return $adapter;
});

/**
 * Setting up the view component
 */
$di->set('view', function() use ($config) {
	$view = new View();
	$view->setViewsDir(__DIR__ . '/api/views/');
	$view->registerEngines(array(
		'.volt' => function($view, $di) use ($config) {
			$volt = new View\Engine\Volt($view, $di);
//			$voltOptions = array(
//				'compiledPath' => $config->application->cacheDir,
//				'compiledSeparator' => '_'
//			);
			//debug
//			if ('1' != $config->application->debug) {
//				$voltOptions['compileAlways'] = true;
//			}
//			$volt->setOptions($voltOptions);
			return $volt;
		},
		'.phtml' => 'Phalcon\Mvc\View\Engine\Php'
	));
	return $view;
}, true);


/**
 * Mail service uses AmazonSES
 */
$di->set('mail', function(){
	return new Mail();
});

$loader->registerNamespaces(
	[
		'api'               => 'api/',
		'api/collections'   => 'api/collections',
		'api/entitys'       => 'api/entitys/',
		'api/controller'    => 'api/controller/',
		'api/model'         => 'api/model/',
		'api/common'        => 'api/common/',
		'api/auth'        	=> 'api/auth/',
		'api/security'     	=> 'api/security/',
		'Firebase\JWT'      => 'api/auth/vendor/firebase/php-jwt/src',
		'api/vendor'		=> 'api/vendor',
		'api/views'			=> 'api/views'
	]
);

// Register some directories
$loader->registerDirs(
	[
		"api/",
		"api/collections/",
		"api/entitys/",
		"api/controller/",
		"api/model/",
		"api/common/",
		"api/auth/",
		"api/security/",
		"api/vendor/",
		"api/views/"
	]
);

//register autoloader
$loader->register();

$API = \api\Api::getInstance($di);

$API->sendRequest();

