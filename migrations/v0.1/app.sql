CREATE TABLE appoinment_status
(
    id INT(10) PRIMARY KEY NOT NULL,
    description VARCHAR(100) NOT NULL
);
CREATE TABLE appointments
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    date_appointment DATETIME NOT NULL,
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    status_appointment_id INT(10) NOT NULL,
    CONSTRAINT appointment_status_fk FOREIGN KEY (status_appointment_id) REFERENCES appoinment_status (id)
);
CREATE INDEX fk_appointments_1_idx ON appointments (status_appointment_id);
CREATE TABLE clinic_professional
(
    clinic_id BIGINT(20),
    professional_id BIGINT(20),
    CONSTRAINT clinic_fk FOREIGN KEY (clinic_id) REFERENCES clinics (id),
    CONSTRAINT professional__fk FOREIGN KEY (professional_id) REFERENCES professionals_professional_registration (id)
);
CREATE INDEX clinic_fk ON clinic_professional (clinic_id);
CREATE INDEX professional__fk ON clinic_professional (professional_id);
CREATE TABLE clinics
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    business_name VARCHAR(255) NOT NULL,
    tax_number INT(10) unsigned NOT NULL,
    phone INT(10) unsigned NOT NULL,
    status TINYINT(3) unsigned NOT NULL
);
CREATE UNIQUE INDEX phone_UNIQUE ON clinics (phone);
CREATE UNIQUE INDEX tax_number_UNIQUE ON clinics (tax_number);
CREATE TABLE medical_insurances
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    description VARCHAR(100) NOT NULL,
    status TINYINT(4) NOT NULL
);
CREATE TABLE pacient_appointment
(
    clinic_id BIGINT(20) NOT NULL,
    professional_id BIGINT(20) NOT NULL,
    appointment_id BIGINT(20) NOT NULL,
    pacient_id BIGINT(20) NOT NULL,
    CONSTRAINT pacient_fk FOREIGN KEY (pacient_id) REFERENCES pacients_insurance (id),
    CONSTRAINT professional_fk FOREIGN KEY (professional_id) REFERENCES professionals_professional_registration (id),
    CONSTRAINT appointment_fk FOREIGN KEY (appointment_id) REFERENCES appointments (id)
);
CREATE INDEX professional_fk ON pacient_appointment (professional_id);
CREATE UNIQUE INDEX uid_appointment ON pacient_appointment (pacient_id, appointment_id);
CREATE INDEX appointment_fk_idx ON pacient_appointment (appointment_id);
CREATE INDEX clinic_fk_idx ON pacient_appointment (clinic_id);
CREATE TABLE pacients
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    uid BIGINT(20) NOT NULL,
    insurance_id BIGINT(20) NOT NULL,
    CONSTRAINT fk_pacient_1 FOREIGN KEY (uid) REFERENCES users (id),
    CONSTRAINT fk_insurance_1 FOREIGN KEY (insurance_id) REFERENCES medical_insurances (id)
);
CREATE INDEX fk_pacient_1_idx ON pacients (uid);
CREATE INDEX fk_insurance_1_idx ON pacients (insurance_id);
CREATE TABLE permission
(
    id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(100) NOT NULL,
    action VARCHAR(100) NOT NULL
);
CREATE UNIQUE INDEX name ON permission (name);
CREATE TABLE professionals
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    uid BIGINT(20) NOT NULL,
    professional_registration INT(20),
    specialty_id INT(11) NOT NULL,
    CONSTRAINT fk_professional_1 FOREIGN KEY (uid) REFERENCES users (id),
    CONSTRAINT specialty_fk FOREIGN KEY (specialty_id) REFERENCES dict_specialty (id)
);
CREATE INDEX fk_professional_1_idx ON professionals (uid);
CREATE UNIQUE INDEX professional_registration_UNIQUE ON professionals (professional_registration);
CREATE INDEX specialty_fk ON professionals (specialty_id);
CREATE TABLE role
(
    id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(20) NOT NULL,
    description VARCHAR(100) NOT NULL,
    enabled TINYINT(4) NOT NULL
);
CREATE UNIQUE INDEX name ON role (name);
CREATE TABLE role_permission
(
    role_id INT(11),
    permission_id INT(11),
    CONSTRAINT role_fk FOREIGN KEY (role_id) REFERENCES role (id),
    CONSTRAINT permission__fk FOREIGN KEY (permission_id) REFERENCES permission (id)
);
CREATE INDEX permission_fk ON role_permission (role_id);
CREATE INDEX permission__fk ON role_permission (permission_id);
CREATE TABLE role_user
(
    role_id INT(11) PRIMARY KEY NOT NULL,
    user_id BIGINT(20) NOT NULL,
    CONSTRAINT fk_role FOREIGN KEY (role_id) REFERENCES role (id),
    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users (id)
);
CREATE INDEX fk_user_idx ON role_user (user_id);
CREATE TABLE specialty
(
    id INT(11) PRIMARY KEY NOT NULL,
    description VARCHAR(100) NOT NULL
);
CREATE TABLE ubication_clinic
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    ubication POINT NOT NULL,
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    clinic_id BIGINT(20) NOT NULL,
    CONSTRAINT ubication_clinic_fk FOREIGN KEY (clinic_id) REFERENCES clinics (id)
);
CREATE INDEX ubication_clinic_fk ON ubication_clinic (clinic_id);
CREATE TABLE ubication_user
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    ubication POINT NOT NULL,
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    date_updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    user_id BIGINT(20) NOT NULL,
    CONSTRAINT ubication_user_fk FOREIGN KEY (user_id) REFERENCES users (id)
);
CREATE INDEX ubication_user_fk ON ubication_user (user_id);
CREATE TABLE users
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(200) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    name VARCHAR(100) NOT NULL,
    du INT(50) NOT NULL,
    phone INT(10) unsigned NOT NULL,
    email VARCHAR(250) NOT NULL,
    birthdate DATETIME NOT NULL,
    status TINYINT(2) unsigned NOT NULL
);
CREATE UNIQUE INDEX phone_UNIQUE ON users (phone);
CREATE UNIQUE INDEX username_UNIQUE ON users (username);