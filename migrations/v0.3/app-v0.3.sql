-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: appointments
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appoinment_status`
--

DROP TABLE IF EXISTS `appoinment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appoinment_status` (
  `id` int(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appoinment_status`
--

LOCK TABLES `appoinment_status` WRITE;
/*!40000 ALTER TABLE `appoinment_status` DISABLE KEYS */;
INSERT INTO `appoinment_status` VALUES (1,'PENDIENTE'),(2,'EN CURSO'),(3,'FINALIZADO'),(4,'CANCELADO');
/*!40000 ALTER TABLE `appoinment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointments` (
  `id` bigint(20) NOT NULL,
  `uid` bigint(20) DEFAULT NULL,
  `pid` bigint(20) NOT NULL,
  `date_appointment` datetime NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_appointment_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `appointments_uid_pid_date_appointment_uindex` (`uid`,`pid`,`date_appointment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_appointment`
--

DROP TABLE IF EXISTS `clinic_appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_appointment` (
  `clinic_id` bigint(20) NOT NULL,
  `appointment_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_appointment`
--

LOCK TABLES `clinic_appointment` WRITE;
/*!40000 ALTER TABLE `clinic_appointment` DISABLE KEYS */;
/*!40000 ALTER TABLE `clinic_appointment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-04 13:04:50
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: findermed
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clinic_professional`
--

DROP TABLE IF EXISTS `clinic_professional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_professional` (
  `clinic_id` bigint(20) DEFAULT NULL,
  `professional_id` bigint(20) DEFAULT NULL,
  KEY `clinic_fk` (`clinic_id`),
  CONSTRAINT `clinic_fk` FOREIGN KEY (`clinic_id`) REFERENCES `clinics` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_professional`
--

LOCK TABLES `clinic_professional` WRITE;
/*!40000 ALTER TABLE `clinic_professional` DISABLE KEYS */;
INSERT INTO `clinic_professional` VALUES (1,4);
/*!40000 ALTER TABLE `clinic_professional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinics`
--

DROP TABLE IF EXISTS `clinics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinics` (
  `id` bigint(20) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `tax_number` varchar(12) NOT NULL,
  `phone` int(10) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_number_UNIQUE` (`tax_number`),
  UNIQUE KEY `phone_UNIQUE` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinics`
--

LOCK TABLES `clinics` WRITE;
/*!40000 ALTER TABLE `clinics` DISABLE KEYS */;
INSERT INTO `clinics` VALUES (1,'Trinidad','3010968544',1140989000,1),(2,'Ineba','3020900000',1130876000,1);
/*!40000 ALTER TABLE `clinics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dict_specialty`
--

DROP TABLE IF EXISTS `dict_specialty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dict_specialty` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dict_specialty`
--

LOCK TABLES `dict_specialty` WRITE;
/*!40000 ALTER TABLE `dict_specialty` DISABLE KEYS */;
INSERT INTO `dict_specialty` VALUES (1,'CARDIOLOGIA'),(2,'TRAUMATOLOGIA'),(3,'ODENTOLOGIA'),(4,'CLINICO'),(5,'GINECOLOGIA');
/*!40000 ALTER TABLE `dict_specialty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical_insurances`
--

DROP TABLE IF EXISTS `medical_insurances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medical_insurances` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_insurances`
--

LOCK TABLES `medical_insurances` WRITE;
/*!40000 ALTER TABLE `medical_insurances` DISABLE KEYS */;
INSERT INTO `medical_insurances` VALUES (1,'OSDE',1),(2,'GALENO',1);
/*!40000 ALTER TABLE `medical_insurances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_hour_zones`
--

DROP TABLE IF EXISTS `professional_hour_zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional_hour_zones` (
  `pid` bigint(20) NOT NULL,
  `clinic_id` bigint(20) NOT NULL,
  `day` tinyint(1) NOT NULL,
  `hour_start` time NOT NULL,
  `hour_end` time NOT NULL,
  `break_time` varchar(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_hour_zones`
--

LOCK TABLES `professional_hour_zones` WRITE;
/*!40000 ALTER TABLE `professional_hour_zones` DISABLE KEYS */;
INSERT INTO `professional_hour_zones` VALUES (4,2,4,'18:01:00','21:00:00','14:30-15:00',1),(4,2,2,'10:00:00','18:00:00','13:30-14:00',1),(4,2,3,'13:00:00','18:00:00','14:00-17:00',1),(4,3,4,'18:01:00','21:00:00','14:30-15:00',1),(4,3,2,'10:00:00','18:00:00','13:30-14:00',1),(4,3,3,'13:00:00','18:00:00','14:00-17:00',1),(4,1,6,'18:01:00','21:00:00','14:30-15:00',1),(4,1,7,'10:00:00','18:00:00','13:30-14:00',1),(4,1,1,'13:00:00','18:00:00','14:00-17:00',1),(4,1,7,'07:00:00','09:00:00','13:30-14:00',1),(4,1,7,'05:00:00','06:59:00','13:30-14:00',1),(4,2,7,'19:00:00','20:59:00','13:30-14:00',1);
/*!40000 ALTER TABLE `professional_hour_zones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `security_clinic`
--

DROP TABLE IF EXISTS `security_clinic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `security_clinic` (
  `clinic_id` bigint(20) NOT NULL,
  `token` varchar(32) NOT NULL,
  UNIQUE KEY `security_clinic_clinic_id_uindex` (`clinic_id`),
  UNIQUE KEY `security_clinic_token_uindex` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `security_clinic`
--

LOCK TABLES `security_clinic` WRITE;
/*!40000 ALTER TABLE `security_clinic` DISABLE KEYS */;
/*!40000 ALTER TABLE `security_clinic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_query`
--

DROP TABLE IF EXISTS `type_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_query` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_query`
--

LOCK TABLES `type_query` WRITE;
/*!40000 ALTER TABLE `type_query` DISABLE KEYS */;
INSERT INTO `type_query` VALUES (1,'SOBRETURNO'),(2,'TURNO GENERAL'),(3,'TURNO EXTENDIDO'),(4,'TURNO CORTO');
/*!40000 ALTER TABLE `type_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `du` int(50) NOT NULL,
  `phone` int(10) unsigned NOT NULL,
  `email` varchar(250) NOT NULL,
  `birthdate` datetime NOT NULL,
  `usertype` tinyint(1) NOT NULL,
  `specialtyId` int(11) DEFAULT NULL,
  `professionalRegistration` int(20) DEFAULT NULL,
  `insuranceId` int(11) DEFAULT NULL,
  `status` tinyint(2) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'idadone','7ada99a2a298eff813e5ecf8a41ed572','Dadone','Irina',29908088,1130876001,'idadone@hotmail.com','1980-05-05 00:00:00',0,1,12345,NULL,0),(2,'nicoksq','7ada99a2a298eff813e5ecf8a41ed572','Andreoli','Nicolas',35908088,1130876000,'nicolasandreoli3@hotmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(3,'mand.mda','7ada99a2a298eff813e5ecf8a41ed572','Andreoli','Moira',29908087,1130876021,'mand.mda@hotmail.com','1980-05-05 00:00:00',0,2,32455,NULL,0),(4,'rulo.marino','7ada99a2a298eff813e5ecf8a41ed572','Marino','Raul',29908067,1130876121,'raul.marino@hotmail.com','1980-05-05 00:00:00',0,3,132425,NULL,0),(5,'pablo.pollito','7ada99a2a298eff813e5ecf8a41ed572','Mealla','Pablo',32908082,1130866001,'pablo.mealla@yahoo.com','1990-05-05 00:00:00',1,NULL,NULL,2,0),(6,'martin.pilcha','7ada99a2a298eff813e5ecf8a41ed572','Alum','Martin',32908083,1130866003,'martin.alum@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,2,0),(7,'vane.valdez','7ada99a2a298eff813e5ecf8a41ed572','Valdez','Vane',32908023,1130836003,'vane.valdez@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(8,'lemon.limon','7ada99a2a298eff813e5ecf8a41ed572','Rosso','Gaston',32908022,1130833003,'gaston.rosso@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(9,'gra.jackson','7ada99a2a298eff813e5ecf8a41ed572','Jackson','Graciela',32938022,1130833203,'graciela.jackson@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(10,'nelson.mandela','palmaspalmas','Mandela','NElson',32938222,1130833123,'nelson.mandela@gmail.com','1990-05-05 00:00:00',0,4,132425,NULL,0),(11,'nelson.mandela2','palmaspalmas','Mandela','NElson',32938223,1130833122,'nelson.mandela2@gmail.com','1990-05-05 00:00:00',0,4,132425,NULL,NULL),(12,'nelson.mandela3','palmaspalmas','Mandela','NElson',32938224,1130833125,'nelson.mandela3@gmail.com','1990-05-05 00:00:00',0,4,132425,NULL,NULL),(13,'damian.nieva','palmaspalmas','Nieva','Damian',32938032,1132833203,'dmn.nie@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(14,'damian.nieva2','palmaspalmas','Nieva','Damian',33938022,1132833103,'dmn.nie3@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(15,'damian.nieva3','palmaspalmas','Nieva','Damian',34938022,1232833103,'dn.nie3@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(16,'damian.nieva4','palmaspalmas','Nieva','Damian',34968022,1452833103,'dn.nie53@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(17,'damian.nieva5','palmaspalmas','Nieva','Damian',34968032,1454833103,'dnnie53@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(18,'damian.nieva6','palmaspalmas','Nieva','Damian',34968232,1454833303,'dnnie52@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(19,'damdan.nieva6','palmaspalmas','Nieva','Damian',34968231,1454333303,'dnn1e56@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(20,'damdan.nieva67','palmaspalmas','Nieva','Damian',34968239,1454393303,'dnn1e596@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0),(21,'damdan.nieva63','palmaspalmas','Nieva','Damian',34968731,1454193303,'dnn2e596@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-04 13:04:50
