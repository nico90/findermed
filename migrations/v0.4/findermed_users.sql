-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: findermed
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(12) NOT NULL,
  `phone` int(10) unsigned NOT NULL,
  `email` varchar(250) NOT NULL,
  `birthdate` datetime NOT NULL,
  `usertype` tinyint(1) NOT NULL,
  `specialtyId` int(11) DEFAULT NULL,
  `professionalRegistration` int(20) DEFAULT NULL,
  `insuranceId` int(11) DEFAULT NULL,
  `status` tinyint(2) unsigned DEFAULT '0',
  `du` int(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'idadone','7ada99a2a298eff813e5ecf8a41ed572','Dadone','Irina','0',1130876001,'idadone@hotmail.com','1980-05-05 00:00:00',0,1,12345,NULL,0,29908088),(2,'nicoksq','7ada99a2a298eff813e5ecf8a41ed572','Andreoli','Nicolas','0',1130876000,'nicolasandreoli3@hotmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,35908088),(3,'mand.mda','7ada99a2a298eff813e5ecf8a41ed572','Andreoli','Moira','0',1130876021,'mand.mda@hotmail.com','1980-05-05 00:00:00',0,2,32455,NULL,0,29908087),(4,'rulo.marino','7ada99a2a298eff813e5ecf8a41ed572','marino3','Raul','0',1130876121,'raul.marino@hotmail.com','1980-05-05 00:00:00',0,3,132425,NULL,0,29908067),(5,'pablo.pollito','7ada99a2a298eff813e5ecf8a41ed572','Mealla','Pablo','0',1130866001,'pablo.mealla@yahoo.com','1990-05-05 00:00:00',1,NULL,NULL,2,0,32908082),(6,'martin.pilcha','7ada99a2a298eff813e5ecf8a41ed572','Alum','Martin','0',1130866003,'martin.alum@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,2,0,32908083),(7,'vane.valdez','7ada99a2a298eff813e5ecf8a41ed572','Valdez','Vane','0',1130836003,'vane.valdez@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,32908023),(8,'lemon.limon','7ada99a2a298eff813e5ecf8a41ed572','Rosso','Gaston','0',1130833003,'gaston.rosso@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,32908022),(9,'gra.jackson','7ada99a2a298eff813e5ecf8a41ed572','Jackson','Graciela','0',1130833203,'graciela.jackson@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,32938022),(10,'nelson.mandela','palmaspalmas','Mandela','NElson','0',1130833123,'nelson.mandela@gmail.com','1990-05-05 00:00:00',0,4,132425,NULL,0,32938222),(11,'nelson.mandela2','palmaspalmas','Mandela','NElson','0',1130833122,'nelson.mandela2@gmail.com','1990-05-05 00:00:00',0,4,132425,NULL,NULL,32938223),(12,'nelson.mandela3','palmaspalmas','Mandela','NElson','0',1130833125,'nelson.mandela3@gmail.com','1990-05-05 00:00:00',0,4,132425,NULL,NULL,32938224),(13,'damian.nieva','palmaspalmas','Nieva','Damian','0',1132833203,'dmn.nie@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,32938032),(14,'damian.nieva2','palmaspalmas','Nieva','Damian','0',1132833103,'dmn.nie3@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,33938022),(15,'damian.nieva3','palmaspalmas','Nieva','Damian','0',1232833103,'dn.nie3@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34938022),(16,'damian.nieva4','palmaspalmas','Nieva','Damian','0',1452833103,'dn.nie53@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34968022),(17,'damian.nieva5','palmaspalmas','Nieva','Damian','0',1454833103,'dnnie53@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34968032),(18,'damian.nieva6','palmaspalmas','Nieva','Damian','0',1454833303,'dnnie52@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34968232),(19,'damdan.nieva6','palmaspalmas','Nieva','Damian','0',1454333303,'dnn1e56@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34968231),(20,'damdan.nieva67','palmaspalmas','Nieva','Damian','0',1454393303,'dnn1e596@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34968239),(21,'damdan.nieva63','palmaspalmas','Nieva','Damian','0',1454193303,'dnn2e596@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34968731),(22,'damdan.nieva13','palmaspalmas','Nieva','Damian','0',1454193103,'dnn2e5964@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34968721),(23,'damdan.nieva133','palmaspalmas','Nieva','Damian','0',1454193203,'dnn2e53964@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,34968221),(24,'damdan.nieva1s33','palmaspalmas','Nieva','Damian','0',1454193901,'dnn2e539624@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,31968221),(25,'damdan.nieva1s333','palmaspalmas','Nieva','Damian','0',1454133901,'dnn2e5393624@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,31963321),(26,'damdan.nieva1ss333','palmaspalmas','Nieva','Damian','0',1455133901,'dnn2e53933624@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,31963341),(27,'damdan.nieva1ss3d33','palmaspalmas','Nieva','Damian','0',1452233901,'dnn2e539336324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,31963241),(28,'damdan.nieva1ss32d33','palmaspalmas','Nieva','Damian','0',1452233904,'dnn2e5393356324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,319632434),(29,'damdan.nieva1ss32d233','palmaspalmas','Nieva','Damian','0',1432233904,'dnn2e53933s56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,319632414),(30,'damdan.nieva1sds32d233','palmaspalmas','Nieva','Damian','0',1132233904,'dnn2e533933s56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,319232414),(31,'damdan.nieva1sss32d233','palmaspalmas','Nieva','Damian','0',1132233924,'dnn2e533932s56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,319232424),(32,'aamdan.nieva1sss32d233','palmaspalmas','Nieva','Damian','0',232233924,'dnn2e533s932s56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,329232424),(33,'aamdan.nievsa1sss32d233','palmaspalmas','Nieva','Damian','0',232213924,'dnn1e533s932s56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,329232414),(34,'aamdan.nievsa1ssss32d233','palmaspalmas','Nieva','Damian','0',112213924,'dnn1e5313s932s56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,329231114),(35,'aamdan.nievsa1ssss32sd233','palmaspalmas','Nieva','Damian','0',112213124,'dnn1e53132s932s56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,329231124),(36,'aamdan.nievsa1sssss32sd233','palmaspalmas','Nieva','Damian','0',111113124,'dnn1e53132s932ss56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,0,329231111),(37,'aamdan.nievssa1sssss32sd233','palmaspalmas','Nieva','Damian','0',122213124,'dnn1e523132s932ss56324@gmail.com','1990-05-05 00:00:00',1,NULL,NULL,1,1,329231221),(38,'gra.jackson2','palmaspalmas','Jackson','Graciela','0',1132833223,'graciela.jackson9@gmail.com','1990-05-05 00:00:00',0,4,132425,NULL,0,32938232);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-15 18:22:29
